package geom;

import core.algorithm.BiarcApproximator;
import core.algorithm.GreedyApproximator;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Test;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static geom.Context2D.optimalContextFor;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * User: seickelberg
 * Date: 22.07.13
 */
public class Context2DUnitTest {

    private static final int POINT_COUNT = 50;

    private List<Biarc> biarcs;
    private Context2D context;

    @Before
    public void setUpTestData() {
        final List<Point2D.Double> points = new ArrayList<Point2D.Double>(POINT_COUNT);
        for (int i = 0; i < POINT_COUNT; i++) {
            final Random generator = new Random();
            final int scale = generator.nextInt(1000);
            final int xOffset = generator.nextInt(1000);
            final int yOffset = generator.nextInt(1000);
            points.add(new Point2D.Double(xOffset + scale * generator.nextDouble(), yOffset + scale * generator.nextDouble()));
        }
        context = optimalContextFor(points);
        BiarcApproximator approximator = new GreedyApproximator();
        approximator.executeWith(points);
        biarcs = approximator.getApproximation();
    }

    @Test
    public void testContext() {
        assertThat(biarcs.size(), is(greaterThan(0)));
        final List<Biarc> revertedBiarcs = context.getRevertedBiarcList(biarcs);
        assertThat(biarcs.size(), is(revertedBiarcs.size()));
        final int xOffset = context.getxOffset();
        final int yOffset = context.getyOffset();
        for (int i = 0; i < biarcs.size(); i++) {
            final Biarc biarc = biarcs.get(i);
            final Biarc revertedBiarc = revertedBiarcs.get(i);
            assertThat(biarc.getFirstArc().getStartDirection(), is(revertedBiarc.getFirstArc().getStartDirection()));
            assertThat(biarc.getEndDirectionAngle(), nearlyEquals(revertedBiarc.getEndDirectionAngle()));
            assertThat(biarc.getStartPoint().getX(), nearlyEquals(revertedBiarc.getStartPoint().getX() - xOffset));
            assertThat(biarc.getStartPoint().getY(), nearlyEquals(revertedBiarc.getStartPoint().getY() - yOffset));
        }
    }

    private Matcher<Double> nearlyEquals(final double v) {
        return new TypeSafeMatcher<Double>() {
            @Override
            public boolean matchesSafely(Double aDouble) {
                return Utils.nearlyEquals(aDouble, v);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("Should be nearly equal to ").appendValue(v);
            }
        };
    }

    private Matcher<Integer> greaterThan(final int i) {
        return new TypeSafeMatcher<Integer>() {
            @Override
            public boolean matchesSafely(Integer o) {
                return o > i;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("Should be greater than ").appendValue(i);
            }
        };
    }
}
