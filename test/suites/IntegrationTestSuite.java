package suites;

import junit.framework.TestSuite;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ui.ConstructCircleIntegrationTest;
import ui.FindTangentVectorIntegrationTest;
import ui.GetCentroidIntegrationTest;
import ui.GreedyApproximatorIntegrationTest;

import static org.junit.runners.Suite.SuiteClasses;

/**
 * User: seickelberg
 * Date: 24.05.13
 */
@RunWith(Suite.class)
@SuiteClasses({
        GetCentroidIntegrationTest.class,
        FindTangentVectorIntegrationTest.class,
        ConstructCircleIntegrationTest.class,
        GreedyApproximatorIntegrationTest.class
})
public class IntegrationTestSuite extends TestSuite {
}
