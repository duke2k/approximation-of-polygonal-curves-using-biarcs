package suites;

import core.algorithm.FastConvexHullUnitTest;
import core.algorithm.GreedyApproximatorUnitTest;
import geom.Context2DUnitTest;
import junit.framework.TestSuite;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ui.helper.ConstraintUnitTest;

import static org.junit.runners.Suite.SuiteClasses;

/**
 * User: seickelberg
 * Date: 24.05.13
 */
@RunWith(Suite.class)
@SuiteClasses({
        FastConvexHullUnitTest.class,
        GreedyApproximatorUnitTest.class,
        Context2DUnitTest.class,
        ConstraintUnitTest.class
})
public class UnitTestSuite extends TestSuite {
}
