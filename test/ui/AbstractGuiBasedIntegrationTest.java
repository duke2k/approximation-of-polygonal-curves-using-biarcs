package ui;

import com.google.inject.Injector;
import core.GreedyApproximatorModule;
import core.state.BiarcListResultState;
import core.state.PointListBasedState;
import geom.Biarc;
import org.junit.After;
import org.junit.Before;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.util.List;

import static com.google.inject.Guice.createInjector;
import static java.lang.Math.round;
import static java.lang.Thread.sleep;

/**
 * User: seickelberg
 * Date: 13.05.13
 */
public abstract class AbstractGuiBasedIntegrationTest {

    protected PointListBasedState<List<Biarc>> state;
    protected MainWindow mainWindow;

    @Before
    public void setUp() {
        Injector injector = createInjector(new GreedyApproximatorModule());
        state = injector.getInstance(BiarcListResultState.class);
        mainWindow = new MainWindow(state);
    }

    @After
    public void tearDown() throws InterruptedException {
        state.removeAllPoints();
        sleep(500);
        mainWindow.dispose();
        state = null;
    }

    protected void addPointToGui(@Nonnull Point2D point) {
        state.addPoint((int) round(point.getX()), (int) round(point.getY()));
    }

    protected void addPointToGuiAndWait(@Nonnull Point2D point, @Nonnegative long msecs) throws InterruptedException {
        addPointToGui(point);
        Thread.sleep(msecs);
    }

    protected void drawCircleToGui(@Nonnull Ellipse2D circle) {
        Color before = mainWindow.getDrawAreaGraphics().getColor();
        mainWindow.getDrawAreaGraphics().setColor(Color.GREEN);
        mainWindow.getDrawAreaGraphics().drawOval(
                (int) round(circle.getCenterX() - (circle.getWidth() / 2.0)),
                (int) round(circle.getCenterY() - (circle.getHeight() / 2.0)),
                (int) round(circle.getWidth()),
                (int) round(circle.getHeight()));
        mainWindow.repaint();
        mainWindow.getDrawAreaGraphics().setColor(before);
    }

    protected void drawCircleToGuiAndWait(@Nonnull Ellipse2D circle, @Nonnegative long msecs) throws InterruptedException {
        drawCircleToGui(circle);
        Thread.sleep(msecs);
    }

    protected void drawLineToGui(@Nonnull Point2D start, @Nonnull Point2D end) {
        final Color before = mainWindow.getDrawAreaGraphics().getColor();
        int[] xvalues = new int[2];
        int[] yvalues = new int[2];
        xvalues[0] = (int) Math.round(start.getX());
        yvalues[0] = (int) Math.round(start.getY());
        xvalues[1] = (int) Math.round(end.getX());
        xvalues[1] = (int) Math.round(end.getY());
        mainWindow.getDrawAreaGraphics().setColor(Color.GREEN);
        mainWindow.getDrawAreaGraphics().drawPolyline(xvalues, yvalues, 2);
        mainWindow.repaint();
        mainWindow.getDrawAreaGraphics().setColor(before);
    }

    protected void drawLineToGuiAndWait(@Nonnull Point2D start, @Nonnull Point2D end, @Nonnegative long msecs) throws InterruptedException {
        drawLineToGui(start, end);
        Thread.sleep(msecs);
    }

    protected void drawBiarcToGui(@Nonnull Biarc biarc) {
        final Color before = mainWindow.getDrawAreaGraphics().getColor();
        mainWindow.getDrawAreaGraphics().setColor(Color.BLACK);
        biarc.drawIn((Graphics2D) mainWindow.getDrawAreaGraphics());
        mainWindow.repaint();
        mainWindow.getDrawAreaGraphics().setColor(before);
    }

    protected void drawBiarcToGuiAndWait(@Nonnull Biarc biarc, @Nonnegative long msecs) throws InterruptedException {
        drawBiarcToGui(biarc);
        Thread.sleep(msecs);
    }

    protected void repaintGui() {
        mainWindow.repaint();
    }
}
