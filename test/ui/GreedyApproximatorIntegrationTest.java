package ui;

import core.algorithm.GreedyApproximator;
import geom.Biarc;
import org.junit.After;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static core.algorithm.TestingUtils.verifyApproximationFor;
import static java.awt.geom.Point2D.Double;
import static java.lang.Math.PI;
import static java.lang.Math.sin;
import static org.junit.Assert.assertEquals;

/**
 * User: seickelberg
 * Date: 17.07.13
 */
public class GreedyApproximatorIntegrationTest extends AbstractGuiBasedIntegrationTest {

    private GreedyApproximator approximator = new GreedyApproximator();

    @Test
    public void testApproximation() throws InterruptedException {
        // put up test data: sine curve with 200 points
        List<Double> testData = new ArrayList<Double>(200);
        for (double i = 0; i < 2 * PI; i += (PI / 100)) {
            final Double point = new Double(100.0 * i + 20.0, 100.0 * sin(i) + 120.0);
            testData.add(point);
            addPointToGuiAndWait(point, 5L);
        }
        // run approximation
        approximator.executeWith(testData);
        for (Biarc biarc : approximator.getApproximation()) {
            drawBiarcToGuiAndWait(biarc, 200L);
        }
        repaintGui();
        // verify
        verifyApproximationFor(testData, approximator);
        assertEquals(approximator.getStatusInfoText(), "using 5 biarcs");
    }

    @After
    public void clearApproximator() {
        approximator.clear();
    }
}
