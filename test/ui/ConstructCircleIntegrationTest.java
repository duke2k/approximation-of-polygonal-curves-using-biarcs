package ui;

import org.junit.Test;

import java.awt.geom.Ellipse2D;

import static geom.Utils.constructCircleFrom;
import static java.lang.Math.round;
import static java.lang.Thread.sleep;
import static junit.framework.TestCase.assertEquals;

/**
 * User: Samuel
 * Date: 12.05.13
 */
public class ConstructCircleIntegrationTest extends AbstractGuiBasedIntegrationTest {

    @Test
    public void testConstructCircle() throws InterruptedException {
        state.addPoint(200, 100);
        sleep(1000);
        state.addPoint(320, 160);
        sleep(1000);
        state.addPoint(210, 300);
        sleep(1000);
        Ellipse2D circle = constructCircleFrom(
                state.getPoints().get(0),
                state.getPoints().get(1),
                state.getPoints().get(2));
        state.addPoint(
                (int) round(circle.getCenterX()),
                (int) round(circle.getCenterY()));
        drawCircleToGuiAndWait(circle, 1000);
        assertEquals("Wrong radius!", 102.20972431674335, circle.getWidth() / 2.0);
        assertEquals("Wrong center x coordinate!", 225.5128205128205, circle.getCenterX());
        assertEquals("Wrong center y coordinate!", 198.97435897435898, circle.getCenterY());
    }
}
