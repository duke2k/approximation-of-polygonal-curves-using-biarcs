package ui.helper;

import org.junit.Test;

/**
 * User: seickelberg
 * Date: 11.09.13
 */
public class ConstraintUnitTest {

    @Test
    public void testNumberRequiredNormal() throws ConstraintViolatedException {
        Constraint.numberRequired("1.111");
        Constraint.numberRequired("-1.111");
        Constraint.numberRequired("1.111e5");
        Constraint.numberRequired("1112");
    }

    @Test(expected = ConstraintViolatedException.class)
    public void testNumberRequiredError() throws ConstraintViolatedException {
        Constraint.numberRequired("1.111X");
    }
}
