package ui;

import org.junit.Test;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import static geom.Utils.getCentroidOf;
import static java.awt.geom.Point2D.Double;
import static java.util.Collections.emptyList;
import static junit.framework.TestCase.*;

/**
 * User: seickelberg
 * Date: 23.05.13
 */
public class GetCentroidIntegrationTest extends AbstractGuiBasedIntegrationTest {

    @Test
    public void testGetCentroidForLine() throws InterruptedException {
        List<Double> line = new ArrayList<Double>(2);
        line.add(new Double(100, 100));
        line.add(new Double(400, 224));
        for (Point2D point : line) {
            addPointToGuiAndWait(point, 250);
        }
        Point2D centroid = getCentroidOf(line);
        assertNotNull(centroid);
        assertEquals(250.0, centroid.getX());
        assertEquals(162.0, centroid.getY());
        addPointToGuiAndWait(centroid, 1000);

    }

    @Test
    public void testGetCentroidForTriangle() throws InterruptedException {
        List<Double> openPolygon = new ArrayList<Double>(3);
        openPolygon.add(new Double(100, 100));
        openPolygon.add(new Double(200, 150));
        openPolygon.add(new Double(120, 300));
        for (Point2D point : openPolygon) {
            addPointToGuiAndWait(point, 250);
        }
        Double centroid = getCentroidOf(openPolygon);
        assertNotNull(centroid);
        assertEquals(140.0, centroid.getX());
        assertEquals(183.33333333333334, centroid.getY());
        addPointToGuiAndWait(centroid, 1000);
    }

    @Test
    public void testGetCentroidForHexagon() throws InterruptedException {
        List<Double> openPolygon = new ArrayList<Double>(6);
        openPolygon.add(new Double(100, 100));
        openPolygon.add(new Double(200, 100));
        openPolygon.add(new Double(250, 160));
        openPolygon.add(new Double(200, 220));
        openPolygon.add(new Double(100, 220));
        openPolygon.add(new Double(50, 160));
        for (Point2D point : openPolygon) {
            addPointToGuiAndWait(point, 250);
        }
        Double centroid = getCentroidOf(openPolygon);
        assertNotNull(centroid);
        assertEquals(150.0, centroid.getX());
        assertEquals(160.0, centroid.getY());
        addPointToGuiAndWait(centroid, 1000);
    }

    @Test
    public void testGetCentroidForOnlyOnePoint() throws InterruptedException {
        List<Double> onePointOnly = new ArrayList<Double>(1);
        onePointOnly.add(new Double(135, 125));
        addPointToGuiAndWait(onePointOnly.get(0), 250);
        Point2D centroid = getCentroidOf(onePointOnly);
        assertNotNull(centroid);
        assertEquals(135.0, centroid.getX());
        assertEquals(125.0, centroid.getY());
        addPointToGuiAndWait(centroid, 1000);
    }

    @Test
    public void testGetCentroidForNoPoints() {
        List<Double> noPoints = emptyList();
        Point2D centroid = getCentroidOf(noPoints);
        assertNull(centroid);
    }
}
