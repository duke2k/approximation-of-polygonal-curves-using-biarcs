package ui;

import geom.Vector;
import org.junit.Test;

import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;

import static geom.Utils.findTangentVectorOn;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

/**
 * User: seickelberg
 * Date: 13.05.13
 */
public class FindTangentVectorIntegrationTest extends AbstractGuiBasedIntegrationTest {

    @Test
    public void testFindTangentVectorOnCircle() throws InterruptedException {
        Ellipse2D circle = new Ellipse2D.Double();
        circle.setFrameFromCenter(200, 300, 350, 450);
        Point2D point = new Point2D.Double(350, 300);
        addPointToGuiAndWait(point, 1000);
        drawCircleToGuiAndWait(circle, 1000);
        Vector tangentVector = findTangentVectorOn(circle, point);
        assertNotNull(tangentVector);
        assertNotNull(tangentVector.getEnd());
        addPointToGuiAndWait(tangentVector.getEnd(), 1000);
        drawLineToGuiAndWait(point, tangentVector.getEnd(), 1000);
        assertEquals(tangentVector.getEnd().getX(), 350.0);
        assertEquals(tangentVector.getEnd().getY(), 150.0);
    }
}
