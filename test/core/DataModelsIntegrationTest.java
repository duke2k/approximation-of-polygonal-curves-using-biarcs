package core;

import org.junit.Before;
import org.junit.Test;

import static core.SourceDataModel.constructFrom;

/**
 * User: seickelberg
 * Date: 13.08.13
 */
public class DataModelsIntegrationTest {

    private static final String SOURCE_FILE_NAME = "test/resources/ipe/DataModelsIntegrationTestSource.ipe";
    private static final String TARGET_FILE_NAME = "test/resources/ipe/DataModelsIntegrationTestTarget.ipe";

    private SourceDataModel sourceDataModel;

    @Before
    public void setUp() {
        sourceDataModel = constructFrom(SOURCE_FILE_NAME);
    }

    @Test
    public void testDataProcessingAndFlow() {

    }
}
