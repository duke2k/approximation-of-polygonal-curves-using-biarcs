package core.algorithm;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.*;

/**
 * User: seickelberg
 * Date: 13.05.13
 */
public abstract class AbstractApproximatorUnitTest {

    protected List<Point2D.Double> createSineCurveWith200Points() {
        List<Point2D.Double> points = new ArrayList<Point2D.Double>();
        for (double i = 0; i < 2 * PI; i += (PI / 100)) {
            points.add(new Point2D.Double(100.0 * i + 20.0, 100.0 * sin(i) + 120.0));
        }
        return points;
    }

    protected List<Point2D.Double> createCosineCurveWith200Points() {
        List<Point2D.Double> points = new ArrayList<Point2D.Double>();
        for (double i = 0; i < 2 * PI; i += (PI / 100)) {
            points.add(new Point2D.Double(100.0 * i + 20.0, 100.0 * cos(i) + 120.0));
        }
        return points;
    }
}
