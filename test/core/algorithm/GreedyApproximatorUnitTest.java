package core.algorithm;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static core.algorithm.TestingUtils.verifyApproximationFor;
import static java.awt.geom.Point2D.Double;

/**
 * User: Sven
 * Date: 16.07.13
 */
public class GreedyApproximatorUnitTest extends AbstractApproximatorUnitTest {

    private GreedyApproximator approximator;

    @Before
    public void setUp() {
        approximator = new GreedyApproximator();
    }

    @Test
    public void testExecuteWith() {
        List<Double> points = createCosineCurveWith200Points();
        approximator.executeWith(points);
        verifyApproximationFor(points, approximator);
    }

    @After
    public void clearApproximator() {
        approximator.clear();
    }
}
