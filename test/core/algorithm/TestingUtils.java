package core.algorithm;

import geom.Biarc;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

/**
 * User: seickelberg
 * Date: 17.07.13
 */
public class TestingUtils {

    public static void verifyApproximationFor(List<Point2D.Double> points, BiarcApproximator approximator) {
        List<Biarc> biarcs = approximator.getApproximation();
        assertNotNull(biarcs);
        ArrayList<Point2D> tempList = new ArrayList<Point2D>();
        double temp, min = Double.MAX_VALUE;
        for (Point2D.Double i : points) {
            tempList.clear();
            tempList.add(i);
            for (Biarc j : biarcs) {
                temp = BiarcUtils.getDistance(tempList, j);
                if (temp < min) min = temp;
            }
            double maxDistance = 50;
            assertTrue(min <= maxDistance);
        }
    }
}
