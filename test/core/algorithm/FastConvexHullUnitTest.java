package core.algorithm;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Test;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * User: seickelberg
 * Date: 23.05.13
 */
public class FastConvexHullUnitTest {

    @Test
    public void testConvexHullWithNonConvexPolygon() {
        FastConvexHull fastConvexHull = new FastConvexHull();
        List<Point2D.Double> polygon = new ArrayList<Point2D.Double>(8); // nicht-konvexes 8-Eck
        polygon.add(new Point2D.Double(100, 100));
        polygon.add(new Point2D.Double(140, 120));
        polygon.add(new Point2D.Double(200, 100));
        polygon.add(new Point2D.Double(250, 160));
        polygon.add(new Point2D.Double(200, 220));
        polygon.add(new Point2D.Double(175, 205));
        polygon.add(new Point2D.Double(100, 220));
        polygon.add(new Point2D.Double(50, 160));
        List<Point2D.Double> convexHull = fastConvexHull.compute(polygon);
        assertThat(convexHull.size(), is(6));
        assertThat(convexHull.get(0), isEqualTo(polygon.get(7)));
        assertThat(convexHull.get(1), isEqualTo(polygon.get(0)));
        assertThat(convexHull.get(2), isEqualTo(polygon.get(2)));
        assertThat(convexHull.get(3), isEqualTo(polygon.get(3)));
        assertThat(convexHull.get(4), isEqualTo(polygon.get(4)));
        assertThat(convexHull.get(5), isEqualTo(polygon.get(6)));
    }

    @Test
    public void testConvexHullWithConvexPolygon() {
        FastConvexHull fastConvexHull = new FastConvexHull();
        List<Point2D.Double> polygon = new ArrayList<Point2D.Double>(6); // konvexes 6-Eck
        polygon.add(new Point2D.Double(100, 100));
        polygon.add(new Point2D.Double(200, 100));
        polygon.add(new Point2D.Double(250, 160));
        polygon.add(new Point2D.Double(200, 220));
        polygon.add(new Point2D.Double(100, 220));
        polygon.add(new Point2D.Double(50, 160));
        List<Point2D.Double> convexHull = fastConvexHull.compute(polygon);
        assertThat(convexHull.size(), is(6));
        assertThat(convexHull.get(0), isEqualTo(polygon.get(5)));
        assertThat(convexHull.get(1), isEqualTo(polygon.get(0)));
        assertThat(convexHull.get(2), isEqualTo(polygon.get(1)));
        assertThat(convexHull.get(3), isEqualTo(polygon.get(2)));
        assertThat(convexHull.get(4), isEqualTo(polygon.get(3)));
        assertThat(convexHull.get(5), isEqualTo(polygon.get(4)));
    }

    private Matcher<Point2D.Double> isEqualTo(final Point2D.Double point) {
        return new TypeSafeMatcher<Point2D.Double>() {
            @Override
            public boolean matchesSafely(Point2D.Double base) {
                return base.equals(point);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("Should match ").appendValue(point);
            }
        };
    }
}
