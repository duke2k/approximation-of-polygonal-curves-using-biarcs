package geom;

import javax.annotation.Nonnull;
import java.awt.*;
import java.awt.geom.Arc2D;
import java.awt.geom.Point2D;

import static com.google.inject.internal.util.$Preconditions.checkArgument;
import static java.awt.geom.Point2D.Double;
import static java.lang.Math.*;

/**
 * User: georg
 * Date: 04.05.13
 */
public class CircularArc {
    protected Double startPoint;
    protected Double endPoint;
    protected double startDirection;  //in Grad
    protected double endDirection;    //in Grad
    private Arc2D.Double toDraw;
    private Double center;
    private double radius;
    private Graphics2D g2d;
    /**
     * true -> math. positiv (gegen uhrzeigersinn)
     * false -> math. negativ
     */
    private boolean circleDirection;
    private JavaFXBasedVec2d startVec;
    private double centerToEndAngle;

    /**
     * Konstruktor um einen Kreisbogen aus einem Start und Endpunkt
     * mit gegebener Anfangsrichtung  als Vektor zu erzeugen
     *
     * @param sP Startpunkt
     * @param eP Endpunkt
     * @param sD Startrichtung als Vektor
     */
    public CircularArc(Double sP, Double eP, Double sD) {
        //TODO: loesung für sehr entfernten mittelpunkt
        startPoint = sP;
        endPoint = eP;
        // vektor der Tangente am Start des Kreisbogens
        startVec = new JavaFXBasedVec2d(sD.getX(), sD.getY());
        //Start-Richtung in Grad
        startDirection = getAngleFromVector(startVec);
        //Vektor vom Start zum Endpunkt
        JavaFXBasedVec2d startToEndVec = new JavaFXBasedVec2d(endPoint.getX() - startPoint.getX(), endPoint.getY() - startPoint.getY());
        //Punkt in der Mitte auf Gerade zw. Start und Endpunkt
        Double startEndMid = new Double(startPoint.getX() + 0.5 * startToEndVec.x, startPoint.getY() + 0.5 * startToEndVec.y);
        /*
         *  Gerade vom Kreismittelpunkt zum mittleren Punkte d. Konstruktors
         *  Auf dieser Gerade liegen alle Mittelpunkte von allen Kreisen die durch den
         *  Start und Endpunkt gehen
         */
        Line centerToMidPoint = new Line(startEndMid, orthogonalVec2d(startToEndVec));

        Line startToCenter = new Line(startPoint, orthogonalVec2d(startVec));
        //System.out.println("\tstartToCenterVec: ["+orthogonalVec2d(startVec).x+", "+orthogonalVec2d(startVec).y+"]");
        //Mittelpunkt d. Kreises
        center = findIntersection(centerToMidPoint, startToCenter);
        JavaFXBasedVec2d startToCenterJavaFXBasedVec2d = new JavaFXBasedVec2d(center.getX() - startPoint.getX(), center.getY() - startPoint.getY());
        radius = Math.sqrt(startToCenterJavaFXBasedVec2d.x * startToCenterJavaFXBasedVec2d.x + startToCenterJavaFXBasedVec2d.y * startToCenterJavaFXBasedVec2d.y);
        //Richtung des Kreisbogens
        double angleStartToEnd = getAngleFromVector(startToEndVec);
        double startToEndVecToStartVecAngle = startDirection - angleStartToEnd;
        if (startToEndVecToStartVecAngle < 0) {
            startToEndVecToStartVecAngle += 360;
        }
        circleDirection = !(startToEndVecToStartVecAngle > 0 && startToEndVecToStartVecAngle < 180);
        //Erzeugen des inneren Arc2D.Double zum Zeichnen
        setToDraw();
        //setzen von EndDirection
        endDirection = circleDirection ? centerToEndAngle + 90 : centerToEndAngle - 90;
        if (endDirection < 0) {
            endDirection += 360;
        }
        if (endDirection >= 360) {
            endDirection -= 360;
        }
    }

    public void setG2d(Graphics2D g2d) {
        this.g2d = g2d;
    }

    /**
     * Konstruktor um einen Kreisbogen aus einem Start und Endpunkt
     * mit gegebener Anfangsrichtung als Winkel zu erzeugen
     *
     * @param sP Startpunkt
     * @param eP Endpunkt
     * @param sD Startrichtung als Winkel
     */
    public CircularArc(Double sP, Double eP, double sD) {
        startPoint = sP;
        endPoint = eP;
        startDirection = sD;

        // vektor der Tangente am Start des Kreisbogens
        startVec = getDirectionVectorFromAngle(startDirection);
        //Vektor vom Start zum Endpunkt
        final JavaFXBasedVec2d startToEndVec = new JavaFXBasedVec2d(endPoint.getX() - startPoint.getX(), endPoint.getY() - startPoint.getY());
        //Punkt in der Mitte auf Gerade zw. Start und Endpunkt
        final Double startEndMid = new Double(startPoint.getX() + 0.5 * startToEndVec.x, startPoint.getY() + 0.5 * startToEndVec.y);
        /*
         *  Gerade vom Kreismittelpunkt zum mittleren Punkte d. Konstruktors
         *  Auf dieser Gerade liegen alle Mittelpunkte von allen Kreisen die durch den
         *  Start und Endpunkt gehen
         */
        final Line centerToMidPoint = new Line(startEndMid, orthogonalVec2d(startToEndVec));
        final Line startToCenter = new Line(startPoint, orthogonalVec2d(startVec));
        //Mittelpunkt d. Kreises
        center = findIntersection(centerToMidPoint, startToCenter);
        final JavaFXBasedVec2d startToCenterJavaFXBasedVec2d = new JavaFXBasedVec2d(center.getX() - startPoint.getX(), center.getY() - startPoint.getY());
        radius = Math.sqrt(startToCenterJavaFXBasedVec2d.x * startToCenterJavaFXBasedVec2d.x + startToCenterJavaFXBasedVec2d.y * startToCenterJavaFXBasedVec2d.y);
        //Richtung des Kreisbogens
        double angleStartToEnd = getAngleFromVector(startToEndVec);
        double startToEndVecToStartVecAngle = startDirection - angleStartToEnd;
        if (startToEndVecToStartVecAngle < 0) {
            startToEndVecToStartVecAngle += 360;
        }

        circleDirection = !(startToEndVecToStartVecAngle > 0 && startToEndVecToStartVecAngle < 180);
        //Erzeugen des inneren Arc2D.Double zum Zeichnen
        setToDraw();

        //setzen von EndDirection
        endDirection = circleDirection ? centerToEndAngle + 90 : centerToEndAngle - 90;
        if (endDirection < 0) {
            endDirection += 360;
        }
        if (endDirection >= 360) {
            endDirection -= 360;
        }
    }

    /**
     * Zeichnet den Kreisbogen auf das Graphics2D Objekt
     * des Kreisbogens
     */
    public void draw() {
        g2d.draw(toDraw);

        //Start- & Endpunkt
        g2d.fillOval((int) startPoint.getX() - 3, (int) startPoint.getY() - 3, 6, 6);
        g2d.fillOval((int) endPoint.getX() - 3, (int) endPoint.getY() - 3, 6, 6);
    }

    public void drawIn(@Nonnull Graphics2D context) {
        setG2d(context);
        draw();
    }

    /**
     * Methode berechnet aus einer Richtung als Winkel einen
     * Vektor in dieser Richtung der Länge 10. (Ich glaube bei
     * 1 würde genauigkeit verloren gehen)
     *
     * @param directionAngle in Grad
     * @return Vektor in dieser Richtung
     */
    public JavaFXBasedVec2d getDirectionVectorFromAngle(double directionAngle) {
        //Vector will be of lenght 10
        return new JavaFXBasedVec2d(10 * Math.cos(Math.toRadians(directionAngle)), -10 * Math.sin(Math.toRadians(directionAngle)));
    }

    /**
     * Gibt einen zum eingabevektor senkrecht stehenden
     * Vektor zurück
     *
     * @param in JavaFXBasedVec2d eingabevektor
     */
    public JavaFXBasedVec2d orthogonalVec2d(JavaFXBasedVec2d in) {
        //noinspection SuspiciousNameCombination
        return new JavaFXBasedVec2d(in.y, -1 * in.x);
    }

    /**
     * berechnet den Schnittpunkt von 2 Geraden
     *
     * @param a Line
     * @param b Line
     * @return Point2D.Double
     */
    @Nonnull
    public Double findIntersection(@Nonnull Line a, @Nonnull Line b) {
        checkArgument(a.getDirection() != b.getDirection());
        final Double crossPoint;
        double ax, bx, cx, dx, ay, by, cy, dy;
        ax = a.getStart().getX();
        ay = a.getStart().getY();
        bx = a.getDirection().x;
        by = a.getDirection().y;
        cx = b.getStart().getX();
        cy = b.getStart().getY();
        dx = b.getDirection().x;
        dy = b.getDirection().y;

        double s = (cy - ay - ((cx - ax) * by / bx)) / (by * dx / bx - dy);

        crossPoint = new Double(cx + s * dx, cy + s * dy);
        return crossPoint;
    }

    public Double getEndPoint() {
        return endPoint;
    }

    public Double getStartPoint() {
        return startPoint;
    }

    public double getEndDirection() {
        return endDirection;
    }

    public double getStartDirection() {
        return startDirection;
    }

    public void setEndDirection(double direction) {
        endDirection = direction;
    }

    public void setStartDirection(double direction) {
        startDirection = direction;
    }

    public double distanceTo(@Nonnull Point2D point) {
        final double distCircle = abs(point.distance(center) - radius);
        final double distStart = point.distance(startPoint);
        final double distEnd = point.distance(endPoint);
        final double minDistStartEnd = min(distStart, distEnd);
        final double angleStartToEnd = getAngleFromStartToEnd();
        final double angleStartToPoint = getAngleBetween(startPoint, point);
        final double anglePointToEnd = getAngleBetween(point, endPoint);
        boolean isPointInCircleSection = angleStartToPoint <= angleStartToEnd && anglePointToEnd <= angleStartToEnd;
        return isPointInCircleSection ? min(distCircle, minDistStartEnd) : minDistStartEnd;
    }

    public double getAngleFromStartToEnd() {
        return getAngleBetween(startPoint, endPoint);
    }

    private double getAngleBetween(@Nonnull Point2D start, @Nonnull Point2D end) {
        final double distCenterToStart = center.distance(start);
        final double distCenterToEnd = center.distance(end);
        final Double vectorToStart = new Double(start.getX() - center.getX(), start.getY() - center.getY());
        final Double vectorToEnd = new Double(end.getX() - center.getX(), end.getY() - center.getY());
        final double scalarProduct = vectorToStart.getX() * vectorToEnd.getX() + vectorToStart.getY() * vectorToEnd.getY();
        return acos(scalarProduct / abs(distCenterToStart) * abs(distCenterToEnd));
    }

    public double getAngleFromVector(JavaFXBasedVec2d javaFXBasedVec2d) {
        double angle = toDegrees(atan2((-1) * javaFXBasedVec2d.y, javaFXBasedVec2d.x));
        if (angle < 0) {
            angle = 360 + angle;
        }
        return angle;
    }

    private void setToDraw() {
        final JavaFXBasedVec2d centerToStart = new JavaFXBasedVec2d(startPoint.getX() - center.getX(), startPoint.getY() - center.getY());
        final JavaFXBasedVec2d centerToEnd = new JavaFXBasedVec2d(endPoint.getX() - center.getX(), endPoint.getY() - center.getY());
        double centerToStartAngle = getAngleFromVector(centerToStart);
        centerToEndAngle = getAngleFromVector(centerToEnd);
        double angleBetween;
        if (circleDirection) {
            angleBetween = centerToEndAngle - centerToStartAngle;
            if (angleBetween < 0) {
                angleBetween = 360 + angleBetween;
            }
        } else {
            angleBetween = centerToEndAngle - centerToStartAngle;
            if (angleBetween > 0) {
                angleBetween = angleBetween - 360;
            }
        }
        toDraw = new Arc2D.Double(center.x - radius, center.y - radius, 2 * radius, 2 * radius, centerToStartAngle, angleBetween, Arc2D.OPEN);
    }

    /**
     * This method returns the orientation of the arc:
     * 1 for counter-clockwise,
     * -1 for clockwise.
     *
     * @return orientation as one of -1, 1.
     */
    public int getOrientation() {
        return circleDirection ? -1 : 1;
    }

    @Nonnull
    public String getIpeRepresentationBy(@Nonnull final Context2D context) {
        final StringBuilder result = new StringBuilder();
        final int yMaxValue = context.getyMaxValue();
        // Der Vorteil ist, dass die Startrichtung für Ipe nicht benötigt wird und der Radius gleich bleibt. Nur die 3
        // Punkte brauchen wir. :)
        final Double sP = new Double(startPoint.getX(), (startPoint.getY() - yMaxValue) * -1);
        final Double eP = new Double(endPoint.getX(), (endPoint.getY() - yMaxValue) * -1);
        final Double cP = new Double(center.getX(), (center.getY() - yMaxValue) * -1);
        result.append(sP.getX());
        result.append(" ");
        result.append(sP.getY());
        result.append(" m"); // moveTo (starting point of the arc)
        result.append(System.getProperty("line.separator"));
        result.append(radius);
        result.append(" 0 0 ");
        // circle from center with radius; * -1 because we have the opposite orientation now after flipping.
        result.append(radius * getOrientation() * -1);
        result.append(" ");
        result.append(cP.getX());
        result.append(" ");
        result.append(cP.getY()); // center point
        result.append(" ");
        result.append(eP.getX());
        result.append(" ");
        result.append(eP.getY()); // end point
        result.append(" a"); // circular arc: left point, 2x2 matrix with radius, center, right point
        result.append(System.getProperty("line.separator"));
        return result.toString();
    }
}
