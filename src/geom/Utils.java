package geom;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import static com.google.inject.internal.util.$Preconditions.checkArgument;
import static geom.Vector.vector;
import static java.awt.geom.AffineTransform.getRotateInstance;
import static java.awt.geom.Point2D.Double;
import static java.lang.Math.*;

/**
 * User: seickelberg
 * Date: 25.04.13
 */
public class Utils {

    public static final double TOLERANCE = 0.005;

    @Nonnull
    public static Point toAwtPoint(@Nonnull Point2D point) {
        return new Point((int) round(point.getX()), (int) round(point.getY()));
    }

    @Nonnull
    public static Double fromAwtPoint(@Nonnull Point point) {
        return new Double((double) point.x, (double) point.y);
    }

    @Nonnull
    public static List<Point> toAwtPoints(@Nonnull List<Double> points) {
        final List<Point> result = new ArrayList<Point>(points.size());
        for (Point2D point : points) {
            result.add(toAwtPoint(point));
        }
        return result;
    }

    @Nonnull
    public static List<Double> fromAwtPoints(@Nonnull List<Point> points) {
        final List<Double> result = new ArrayList<Double>(points.size());
        for (Point point : points) {
            result.add(fromAwtPoint(point));
        }
        return result;
    }

    public static boolean nearlyEquals(double number1, double number2) {
        final double upperBound1 = number1 + TOLERANCE;
        final double lowerBound1 = number1 - TOLERANCE;
        final double upperBound2 = number2 + TOLERANCE;
        final double lowerBound2 = number2 - TOLERANCE;
        return (number1 < upperBound2 && number1 > lowerBound2) ||
                (number2 < upperBound1 && number2 > lowerBound1);
    }

    @Nonnull
    public static Ellipse2D constructCircleFrom(@Nonnull Point2D point1, @Nonnull Point2D point2, @Nonnull Point2D point3) {
        final Ellipse2D result = new Ellipse2D.Double();
        double[] x = new double[3];
        double[] y = new double[3];
        double[] m = new double[12];
        x[0] = point1.getX();
        x[1] = point2.getX();
        x[2] = point3.getX();
        y[0] = point1.getY();
        y[1] = point2.getY();
        y[2] = point3.getY();
        for (int i = 0; i < 3; i++) {
            int j = i * 4;
            m[j] = 1;
            m[j + 1] = -2 * x[i];
            m[j + 2] = -2 * y[i];
            m[j + 3] = -x[i] * x[i] - y[i] * y[i];
        }
        solveLinear(3, 4, m);
        final double radius = sqrt(m[7] * m[7] + m[11] * m[11] - m[3]);
        result.setFrameFromCenter(m[7], m[11], m[7] + radius, m[11] + radius);
        return result;
    }

    private static void solveLinear(@Nonnegative int rows, @Nonnegative int columns, @Nonnull double[] matrix) {
        checkArgument(matrix.length == (rows * columns));
        int i, j, k;
        double q;
        for (j = 0; j < columns - 1; j++) {
            q = matrix[j * columns + j];
            if (q == 0) {
                for (i = j + 1; i < rows; i++) {
                    if (matrix[i * columns + j] != 0) {
                        for (k = 0; k < columns; k++) {
                            matrix[j * columns + k] += matrix[i * columns + k];
                        }
                        q = matrix[j * columns + j];
                        break;
                    }
                }
            }
            if (q != 0) {
                for (k = 0; k < columns; k++) {
                    matrix[j * columns + k] = matrix[j * columns + k] / q;
                }
            }
            for (i = 0; i < rows; i++) {
                if (i != j) {
                    q = matrix[i * columns + j];
                    for (k = 0; k < columns; k++) {
                        matrix[i * columns + k] -= q * matrix[j * columns + k];
                    }
                }
            }
        }
    }

    @Nullable
    public static Vector findTangentVectorOn(@Nonnull Ellipse2D circle, @Nonnull Point2D pointOnCircle) {
        Vector result = null;
        double epsilon = 5e-10;
        if (circle.intersects(
                pointOnCircle.getX() - epsilon,
                pointOnCircle.getY() - epsilon,
                pointOnCircle.getX() + epsilon,
                pointOnCircle.getY() + epsilon)) {
            AffineTransform affineTransform = getRotateInstance(
                    toRadians(90), pointOnCircle.getX(), pointOnCircle.getY());
            Point2D end = new Double();
            affineTransform.transform(new Double(circle.getCenterX(), circle.getCenterY()), end);
            result = vector().from(pointOnCircle).to(end);
        }
        return result;
    }

    @Nullable
    public static Double getCentroidOf(@Nonnull List<Double> polygon) {
        if (polygon.size() == 1) {
            return polygon.get(0);
        } else if (polygon.size() == 0) {
            return null;
        }
        double cx = 0.0, cy = 0.0;
        if (polygon.size() == 2) {
            cx = (polygon.get(0).getX() + polygon.get(1).getX()) / 2.0;
            cy = (polygon.get(0).getY() + polygon.get(1).getY()) / 2.0;
        } else {
            polygon.add(polygon.get(0));
            double polygonArea = getAreaOf(polygon);
            for (int i = 0; i < polygon.size() - 1; i++) {
                double ix = polygon.get(i).getX();
                double iy = polygon.get(i).getY();
                double ixn = polygon.get(i + 1).getX();
                double iyn = polygon.get(i + 1).getY();
                double cpr = ix * iyn - ixn * iy;
                cx = cx + (ix + ixn) * cpr;
                cy = cy + (iy + iyn) * cpr;
            }
            cx /= (6.0 * polygonArea);
            cy /= (6.0 * polygonArea);
        }
        return new Double(cx, cy);
    }

    @Nonnegative
    private static double getAreaOf(@Nonnull List<Double> polygon) {
        double sum = 0.0;
        for (int i = 0; i < polygon.size() - 1; i++) {
            double ix = polygon.get(i).getX();
            double iy = polygon.get(i).getY();
            double ixn = polygon.get(i + 1).getX();
            double iyn = polygon.get(i + 1).getY();
            sum = sum + ix * iyn - iy * ixn;
        }
        return abs(0.5 * sum);
    }
}
