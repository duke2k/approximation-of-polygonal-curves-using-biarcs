package geom;

import javax.annotation.Nonnull;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static com.google.inject.internal.util.$Preconditions.checkArgument;
import static geom.Utils.toAwtPoints;
import static java.awt.geom.Point2D.Double;

/**
 * User: seickelberg
 * Date: 18.04.13
 * <p/>
 * Ipe files can contain pixel positions that are way out of range, so we need to determine a proper translating
 * first, to be able to show the polygon supplied by ipe in our drawing window.
 * In addition, we need to store the original context (instance of this context) to store the approximation in an ipe file
 * relative to its original polygon.
 * <p/>
 * This is a good unit-testable class. ;-)
 */
public class Context2D {

    private int xOffset = 0, yOffset = 0, yMaxValue = 0;
    private boolean isReadFromIpeFile = false;
    private int xMaxValue = 0;

    @Nonnull
    public static Context2D optimalContextFor(@Nonnull List<Double> points) {
        final ContextEvaluator contextEvaluator = new ContextEvaluator(points).invoke();
        final int minX = contextEvaluator.getMinX();
        final int minY = contextEvaluator.getMinY();
        final int maxY = contextEvaluator.getMaxY();
        final int maxX = contextEvaluator.getMaxX();
        final Context2D result = new Context2D();
        result.setxOffset(minX);
        result.setyOffset(minY);
        result.setyMaxValue(maxY);
        result.setxMaxValue(maxX);
        return result;
    }


    public int getxOffset() {
        return xOffset;
    }

    public void setxOffset(int xOffset) {
        this.xOffset = xOffset;
    }

    public int getyOffset() {
        return yOffset;
    }

    public void setyOffset(int yOffset) {
        this.yOffset = yOffset;
    }

    public void setyMaxValue(int yMaxValue) {
        this.yMaxValue = yMaxValue;
    }

    public void setxMaxValue(int xMaxValue) {
        this.xMaxValue = xMaxValue;
    }


    public boolean isReadFromIpeFile() {
        return isReadFromIpeFile;
    }

    public void setReadFromIpeFile(boolean readFromIpeFile) {
        isReadFromIpeFile = readFromIpeFile;
    }

    @Nonnull
    public List<Biarc> getRevertedBiarcList(@Nonnull List<Biarc> biarcs) {
        final List<Biarc> result;
        if (isReadFromIpeFile) {
            result = new ArrayList<Biarc>(biarcs.size());
            for (Biarc biarc : biarcs) {
                if (biarc != null) {
                    CircularArc arc1 = new CircularArc(
                            scalePoint(biarc.getStartPoint()),
                            scalePoint(biarc.getJoint()),
                            biarc.getFirstArc().getStartDirection()
                    );
                    CircularArc arc2 = new CircularArc(
                            scalePoint(biarc.getJoint()),
                            scalePoint(biarc.getEndPoint()),
                            biarc.getSecondArc().getStartDirection()
                    );
                    result.add(new Biarc(arc1, arc2));
                }
            }
        } else {
            result = biarcs;
        }
        return result;
    }

    @Nonnull
    private Double scalePoint(@Nonnull final Double point) {
        final Double result = new Double();
        result.setLocation(point.getX() + xOffset, point.getY() + yOffset);
        return result;
    }

    public int getyMaxValue() {
        return yMaxValue;
    }

    public void update(@Nonnull final List<Double> updatedPointList) {
        final ContextEvaluator contextEvaluator = new ContextEvaluator(updatedPointList).invoke();
        xOffset = contextEvaluator.getMinX();
        yOffset = contextEvaluator.getMinY();
        yMaxValue = contextEvaluator.getMaxY();
    }

    private static class ContextEvaluator {
        private List<Double> points;
        private int minX;
        private int minY;
        private int maxX;
        private int maxY;

        public ContextEvaluator(List<Double> points) {
            this.points = points;
        }

        public int getMinX() {
            return minX;
        }

        public int getMinY() {
            return minY;
        }

        public int getMaxY() {
            return maxY;
        }

        public ContextEvaluator invoke() {
            minX = Integer.MAX_VALUE;
            minY = Integer.MAX_VALUE;
            maxX = 0;
            maxY = 0;
            final List<Point> awtPoints = toAwtPoints(points);
            for (Point point : awtPoints) {

                minX = point.x < minX ? point.x : minX;
                minY = point.y < minY ? point.y : minY;
                maxX = maxX < point.x ? point.x : maxX;
                maxY = maxY < point.y ? point.y : maxY;
            }


            //If the list was empty this crashes!
            checkArgument(minX <= maxX);
            checkArgument(minY <= maxY);
            return this;
        }

        public int getMaxX() {
            return maxX;
        }
    }
}
