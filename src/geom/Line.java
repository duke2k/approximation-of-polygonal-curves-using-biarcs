package geom;

import java.awt.geom.Point2D;

/**
 * User: georg
 * Date: 04.05.13
 * <p/>
 * Diese Klasse stellt eine Gerade die durch einen
 * Punkt und einen Richtungsvektor eindeutig bestimmt ist
 * dar.
 */
public class Line {
    public Point2D.Double getStart() {
        return start;
    }

    private final Point2D.Double start;

    public JavaFXBasedVec2d getDirection() {
        return direction;
    }

    private final JavaFXBasedVec2d direction;

    /**
     * Erzeugt eine Gerade aus einem Punkt auf der Geraden
     * und einem Richtungsvektor.
     */
    public Line(Point2D.Double s, JavaFXBasedVec2d d) {
        start = s;
        direction = d;
    }
}
