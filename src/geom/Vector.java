package geom;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.awt.geom.Point2D;

public class Vector extends Point2D.Double {

    private Point2D end;

    private Vector() {
        super();
    }

    @Nonnull
    public static Vector vector() {
        return new Vector();
    }

    @Nonnull
    public Vector from(@Nonnull Point2D startPoint) {
        return from(startPoint.getX(), startPoint.getY());
    }

    @Nonnull
    public Vector from(double x, double y) {
        setLocation(x, y);
        return this;
    }

    @Nonnull
    public Vector to(@Nonnull Point2D endPoint) {
        return to(endPoint.getX(), endPoint.getY());
    }

    @Nonnull
    public Vector to(double x, double y) {
        end = new Double(x, y);
        return this;
    }

    public boolean equals(@Nonnull Vector other) {
        return end != null && getX() == other.getX() && getY() == other.getY() && end.getX() == other.getEnd().getX() && end.getY() == other.getEnd().getY();
    }

    @Nullable
    public Point2D getEnd() {
        return end;
    }

    @Nonnegative
    public double length() {
        final double result;
        if (end != null) {
            result = distance(end.getX(), end.getY());
        } else {
            result = 0;
        }
        return result;
    }
}
