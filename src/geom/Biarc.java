package geom;

import javax.annotation.Nonnull;
import java.awt.*;
import java.awt.geom.Point2D;

import static com.google.inject.internal.util.$Preconditions.checkArgument;
import static geom.Utils.nearlyEquals;
import static java.awt.geom.Point2D.Double;
import static java.lang.Math.min;

/**
 * User: seickelberg
 * Date: 03.04.13
 */
public class Biarc {

    private final CircularArc firstArc;
    private final CircularArc secondArc;

    public Biarc(@Nonnull CircularArc firstArc, @Nonnull CircularArc secondArc) {
        checkArgument(firstArc.getEndPoint().getX() == secondArc.getStartPoint().getX());
        checkArgument(firstArc.getEndPoint().getY() == secondArc.getStartPoint().getY());
        checkArgument(nearlyEquals(firstArc.getEndDirection(), secondArc.getStartDirection()));
        this.firstArc = firstArc;
        this.secondArc = secondArc;
    }

    public void drawIn(@Nonnull Graphics2D context) {
        firstArc.drawIn(context);
        secondArc.drawIn(context);
    }

    public Double getStartPoint() {
        return firstArc.getStartPoint();
    }

    public Double getJoint() {
        return firstArc.getEndPoint();
    }

    public double distanceTo(Point2D point) {
        return min(firstArc.distanceTo(point), secondArc.distanceTo(point));
    }

    public Double getEndPoint() {
        return secondArc.getEndPoint();
    }

    public double getEndDirectionAngle() {
        return secondArc.getEndDirection();
    }

    public CircularArc getFirstArc() {
        return firstArc;
    }

    public CircularArc getSecondArc() {
        return secondArc;
    }
}
