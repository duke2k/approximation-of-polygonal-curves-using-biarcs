package io;

import com.google.common.base.Charsets;
import multex.Exc;

import javax.annotation.Nonnull;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.util.EventReaderDelegate;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.LinkedList;

import static core.SourceDataModel.DTD_STRING;
import static io.Utils.checkFileNameAndFormat;

/**
 * User: seickelberg
 * Date: 15.04.13
 */
public class IpeFileReader extends InputStreamReader {

    public static final Charset CHARSET = Charsets.UTF_8;

    public IpeFileReader(@Nonnull String fileName) throws Exc, FileNotFoundException {
        super(new FilterDTDInputStream(new FileInputStream(fileName)), CHARSET);
        checkFileNameAndFormat(fileName);
    }

    /**
     * The xml files from Ipe include a DTD reference, which confuses our xml validator, since it tries to find
     * a DTD which doesn't exist. This is why we include a DTDRemover class, which takes care of that and removes
     * the corresponding <!DOCTYPE> tag.
     */
    public static class DTDRemover extends EventReaderDelegate {

        public DTDRemover(XMLEventReader reader) {
            super(reader);
        }

        @Override
        public XMLEvent nextEvent() throws XMLStreamException {
            XMLEvent xmlEvent = super.nextEvent();
            if (xmlEvent.getEventType() == XMLEvent.DTD) {
                xmlEvent = super.nextEvent();
            }
            return xmlEvent;
        }
    }

    public static class FilterDTDInputStream extends FilterInputStream {

        private final LinkedList<Integer> inQueue = new LinkedList<Integer>();
        private final LinkedList<Integer> outQueue = new LinkedList<Integer>();
        private final byte[] search = DTD_STRING.getBytes(CHARSET);
        private final byte[] replacement = "".getBytes(CHARSET);

        protected FilterDTDInputStream(InputStream in) {
            super(in);
        }

        private boolean isMatchFound() {
            Iterator<Integer> inIter = inQueue.iterator();
            for (byte b : search) {
                if (!inIter.hasNext() || b != inIter.next()) {
                    return false;
                }
            }
            return true;
        }

        private void readAhead() throws IOException {
            while (inQueue.size() < search.length) {
                int next = super.read();
                inQueue.offer(next);
                if (next == -1) {
                    break;
                }
            }
        }

        @Override
        public int read() throws IOException {
            if (outQueue.isEmpty()) {
                readAhead();
                if (isMatchFound()) {
                    for (byte ignored : search) {
                        inQueue.remove();
                    }
                    for (byte b : replacement) {
                        outQueue.offer((int) b);
                    }
                } else {
                    outQueue.add(inQueue.remove());
                }
            }
            return outQueue.remove();
        }
    }
}
