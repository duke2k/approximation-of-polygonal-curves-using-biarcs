package io;

import javax.annotation.Nonnull;
import java.io.FileWriter;
import java.io.IOException;

import static io.Utils.checkFileNameAndFormat;

/**
 * User: seickelberg
 * Date: 15.04.13
 */
public class IpeFileWriter extends FileWriter {

    public IpeFileWriter(@Nonnull String fileName) throws IOException {
        super(fileName);
        checkFileNameAndFormat(fileName);
    }
}
