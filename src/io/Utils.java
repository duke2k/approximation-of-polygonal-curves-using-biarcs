package io;

import multex.Exc;
import multex.Failure;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSResourceResolver;
import org.xml.sax.SAXException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.xml.XMLConstants;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URL;
import java.util.Properties;

import static com.google.inject.internal.util.$Preconditions.checkArgument;
import static io.IpeFileReader.DTDRemover;

/**
 * User: seickelberg
 * Date: 15.04.13
 */
public class Utils {

    public static final String SCHEMA_PATH = resolve("resources/ipe/schema/ipe.xsd");
    public static final String TEMPLATE_PATH = resolve("resources/ipe/template.ipe");

    public static final String PROPERTIES_FILE_NAME = "config.properties";

    private static final Logger LOG = LoggerFactory.getLogger(Utils.class);

    @Nonnull
    public static String resolve(@Nonnull final String givenPath) {
        final String actualPath;
        if (givenPath.startsWith("src/")) {
            actualPath = givenPath.substring(4);
        } else {
            actualPath = "src/" + givenPath;
        }
        final File fileForActualPath = new File(actualPath);
        if (fileForActualPath.exists()) {
            return actualPath;
        }
        return givenPath;
    }

    public static void validateAgainstSchema(@Nonnull String fileName) {
        XMLInputFactory inFactory = XMLInputFactory.newInstance();
        XMLOutputFactory outFactory = XMLOutputFactory.newInstance();
        final Writer stringWriter = createStringWriterFor(fileName, inFactory, outFactory);
        final URL schemaUrl = Utils.class.getResource("/" + SCHEMA_PATH);
        final Validator validator;
        if (schemaUrl == null) {
            validator = getValidatorFor(SCHEMA_PATH, XMLConstants.W3C_XML_SCHEMA_NS_URI);
        } else {
            validator = getValidatorFor(schemaUrl, XMLConstants.W3C_XML_SCHEMA_NS_URI);
        }
        validate(fileName, stringWriter, validator);
    }

    private static void validate(String fileName, Writer stringWriter, Validator validator) {
        validator.setResourceResolver(new LSResourceResolver() {
            @Override
            public LSInput resolveResource(String s, String s2, String s3, String s4, String s5) {
                return null;
            }
        });
        try {
            stringWriter.flush();
            final Reader stringReader = new StringReader(stringWriter.toString());
            final Source source = new StreamSource(stringReader);
            validator.validate(source);
        } catch (Exception e) {
            throw new Exc("Validation of file with name {0} failed.", e, fileName);
        } finally {
            closeQuietly(stringWriter);
        }
    }

    private static Writer createStringWriterFor(String fileName, XMLInputFactory inFactory, XMLOutputFactory outFactory) {
        XMLEventReader reader;
        try {
            reader = new DTDRemover(inFactory.createXMLEventReader(new StreamSource(fileName)));
        } catch (XMLStreamException e) {
            throw new Exc("Validation of file with name {0} failed.", e, fileName);
        }
        final Writer stringWriter = new StringWriter();
        XMLEventWriter xmlEventWriter = null;
        try {
            xmlEventWriter = outFactory.createXMLEventWriter(stringWriter);
            xmlEventWriter.add(reader);
            xmlEventWriter.flush();
        } catch (XMLStreamException e) {
            throw new Exc("Validation of file with name {0} failed.", e, fileName);
        } finally {
            if (xmlEventWriter != null) {
                try {
                    xmlEventWriter.close();
                } catch (XMLStreamException ignored) {
                }
            }
        }
        return stringWriter;
    }

    private static Validator getValidatorFor(@Nonnull URL definitionUrl, @Nonnull String xmlConstantsOption) {
        final SchemaFactory schemaFactory = SchemaFactory.newInstance(xmlConstantsOption);
        final Schema schema;
        try {
            schema = schemaFactory.newSchema(definitionUrl);
        } catch (SAXException e) {
            throw new Failure("Schema/DTD expected at {0} could not be loaded.", e, definitionUrl);
        }
        return schema.newValidator();
    }

    private static Validator getValidatorFor(@Nonnull String definitionPath, @Nonnull String xmlConstantsOption) {
        final SchemaFactory schemaFactory = SchemaFactory.newInstance(xmlConstantsOption);
        final Schema schema;
        try {
            schema = schemaFactory.newSchema(new File(definitionPath));
        } catch (SAXException e) {
            throw new Failure("Schema/DTD expected at {0} could not be loaded.", e, definitionPath);
        }
        return schema.newValidator();
    }

    public static void checkFileNameAndFormat(String fileName) {
        checkArgument(fileName.endsWith(".ipe") || fileName.endsWith(".xml"));
        final File file = new File(fileName);
        if (file.exists() && file.length() > 0) {
            validateAgainstSchema(fileName);
        }
    }

    public static void closeQuietly(@Nullable Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException ignored) {
                // ignore this, if close() fails.
            }
        }
    }

    public static void loadProperties(Properties properties) {
        InputStream propertiesStream = Utils.class.getClassLoader().getResourceAsStream(PROPERTIES_FILE_NAME);
        try {
            if (propertiesStream == null) {
                propertiesStream = new FileInputStream("src/" + PROPERTIES_FILE_NAME);
            }
            properties.load(propertiesStream);
        } catch (Exception e) {
            LOG.warn("Properties file '" + PROPERTIES_FILE_NAME + "' could not be loaded or properties could not be found " +
                    "or classes in properties file do not exist. Using default classes instead.", e);
        } finally {
            closeQuietly(propertiesStream);
        }
    }
}
