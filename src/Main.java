import com.google.inject.Injector;
import core.AbstractApproximatorModule;
import core.GreedyApproximatorModule;
import core.SourceDataModel;
import core.TargetDataModel;
import core.state.BiarcListResultState;
import core.state.PointListBasedState;
import geom.Biarc;
import multex.Msg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ui.MainWindow;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;
import java.awt.geom.Point2D;
import java.util.List;

import static com.google.inject.Guice.createInjector;
import static core.Configuration.findProperty;
import static core.SourceDataModel.constructFrom;
import static core.TargetDataModel.fromBiarcSequenceAndContext;
import static java.lang.Class.forName;
import static java.lang.Double.parseDouble;
import static java.lang.Math.round;

/**
 * User: seickelberg
 * Date: 03.04.13
 */
public class Main {

    public static final Class<? extends PointListBasedState> DEFAULT_STATE_CLASS = BiarcListResultState.class;
    public static final Class<? extends AbstractApproximatorModule> DEFAULT_APPROXIMATOR_MODULE_CLASS = GreedyApproximatorModule.class;

    private static Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        Class<? extends PointListBasedState> stateClass;
        Class<? extends AbstractApproximatorModule> approximatorModuleClass;
        try {
            //noinspection unchecked
            stateClass = (Class<? extends PointListBasedState>) forName(findProperty("state"));
            //noinspection unchecked
            approximatorModuleClass = (Class<? extends AbstractApproximatorModule>) forName(findProperty("approximatorModule"));
        } catch (ClassNotFoundException e) {
            stateClass = DEFAULT_STATE_CLASS;
            approximatorModuleClass = DEFAULT_APPROXIMATOR_MODULE_CLASS;
            LOG.warn("Invalid class name(s) defined in properties. See attached exception for details.", e);
        }
        final Injector injector;
        try {
            injector = createInjector(approximatorModuleClass.newInstance());
            final PointListBasedState state = injector.getInstance(stateClass);
            state.getApproximator().setMaxDistance(parseDouble(findProperty("maxDistance")));
            if (args.length == 3) {
                // command line call: 'java -jar biarcs.jar [maxDistance] [sourceIpeFileName] [targetIpeFileName]'
                state.getApproximator().setMaxDistance(parseDouble(args[0]));
                final Runnable runnable = new CommandLineRunner(
                        args[1],
                        args[2],
                        state
                );
                runnable.run();
            } else {
                // GUI call: MainWindow gets the predefined state from the injector, as well as an optional file, if
                //           supplied in the call 'java -jar biarcs.jar [sourceFileName]'
                new MainWindow(state, args.length == 1 ? args[0] : null);
            }
        } catch (Exception e) {
            LOG.error("Approximator module could not be instantiated!", e);
            Msg.printReport(e);
        }
    }

    @ThreadSafe
    protected static class CommandLineRunner implements Runnable {

        private String sourceIpeFileName;
        private String targetIpeFileName;
        private PointListBasedState state;

        public CommandLineRunner(
                @Nonnull String sourceIpeFileName,
                @Nonnull String targetIpeFileName,
                @Nonnull PointListBasedState state
        ) {
            this.sourceIpeFileName = sourceIpeFileName;
            this.targetIpeFileName = targetIpeFileName;
            this.state = state;
        }

        @Override
        public void run() {
            final SourceDataModel sourceDataModel = constructFrom(sourceIpeFileName);
            for (Point2D point : sourceDataModel.getPointList()) {
                state.addPoint((int) round(point.getX()), (int) round(point.getY()));
            }
            state.approximate();
            //noinspection unchecked
            final List<Biarc> approximation = (List<Biarc>) state.getApproximator().getApproximation();
            final TargetDataModel targetDataModel = fromBiarcSequenceAndContext(approximation, sourceDataModel.getContext());
            targetDataModel.createAndSaveAsIpeDrawing(targetIpeFileName);
        }
    }
}
