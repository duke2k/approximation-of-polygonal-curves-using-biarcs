package ui;

import com.google.inject.Binding;
import com.google.inject.Injector;
import core.AbstractApproximatorModule;
import core.Configuration;
import core.algorithm.BiarcApproximator;
import core.state.BiarcListResultState;
import core.state.PointListBasedState;
import ui.helper.Constraint;
import ui.helper.Layout;

import javax.annotation.Nonnull;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import static com.google.inject.Guice.createInjector;
import static core.Configuration.findProperty;
import static java.lang.Boolean.parseBoolean;
import static java.lang.Class.forName;
import static java.lang.Double.parseDouble;
import static multex.Swing.report;

/**
 * User: seickelberg
 * Date: 10.09.13
 */
public class OptionsDialog extends JDialog {

    private final List<String> friendlyModuleNames;
    private final PointListBasedState<?> state;
    private final List<JRadioButton> availableApproximatorRadioButtons = new ArrayList<JRadioButton>();

    private JTextField maximumDistanceTextField = new JTextField(findProperty("maxDistance"));
    private JCheckBox alsoExportToJsonCheckBox = new JCheckBox("Also export to JSON file format", parseBoolean(findProperty("alsoExportToJson")));
    private JCheckBox withDistanceCriterionCheckBox = new JCheckBox("Use distance criterion", parseBoolean(findProperty("withDistanceCriterion")));

    public OptionsDialog(List<String> friendlyModuleNames, JFrame parent, PointListBasedState<?> state) {
        super(parent);
        this.friendlyModuleNames = friendlyModuleNames;
        this.state = state;
        setTitle("Options");
        setBounds(parent.getX(), parent.getY(), parent.getWidth() / 2, parent.getHeight() / 2);
        final Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());
        addTextFields();
        addButtonBar();
    }

    private void addButtonBar() {
        ButtonPanel buttonBar = new ButtonPanel();
        JButton okButton = new JButton("OK");
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                onOk();
            }
        });
        buttonBar.add(okButton);
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });
        buttonBar.add(cancelButton);
        add(buttonBar, BorderLayout.SOUTH);
    }

    private void onCancel() {
        setVisible(false);
        dispose();
    }

    private void onOk() {
        final Configuration configuration = Configuration.getInstance();
        configuration.set("alsoExportToJson", String.valueOf(alsoExportToJsonCheckBox.isSelected()));
        configuration.set("withDistanceCriterion", String.valueOf(withDistanceCriterionCheckBox.isSelected()));
        try {
            Constraint.numberRequired(maximumDistanceTextField.getText());
            configuration.set("maxDistance", maximumDistanceTextField.getText());
            updateState();
            onCancel();
        } catch (Exception e) {
            report(this, e);
        }
    }

    private void updateState() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        state.getApproximator().clear();
        if (!(state.getApproximator().getClass().getName().endsWith(getSelectedApproximatorModuleName()))) {
            //noinspection unchecked
            final Class<? extends AbstractApproximatorModule> approximatorModuleClass =
                    (Class<? extends AbstractApproximatorModule>) forName(getSelectedApproximatorModuleName());
            final Injector injector = createInjector(approximatorModuleClass.newInstance());
            final Binding<BiarcApproximator> binding = injector.getBinding(BiarcApproximator.class);
            ((BiarcListResultState) state).setApproximator(binding.getProvider().get());
        }
        state.getApproximator().setMaxDistance(parseDouble(maximumDistanceTextField.getText()));
        if (!state.getPoints().isEmpty()) {
            state.approximate();
        }
    }

    @Nonnull
    private String getSelectedApproximatorModuleName() {
        String result = findProperty("approximatorModule");
        for (JRadioButton radioButton : availableApproximatorRadioButtons) {
            if (radioButton.isSelected()) {
                result = radioButton.getText();
                break;
            }
        }
        //noinspection ConstantConditions
        return result;
    }

    private void addTextFields() {
        final JPanel fieldsPanel = new JPanel(new GridBagLayout());
        fieldsPanel.add(new JLabel("Available heuristics: "), Layout.getLeft(0, 0));
        int i = 0;
        for (final String name : friendlyModuleNames) {
            final JRadioButton radioButton = new JRadioButton(name);
            //noinspection ConstantConditions
            radioButton.setSelected(findProperty("approximatorModule").endsWith(name));
            fieldsPanel.add(radioButton, Layout.getLeft(1, i++));
            availableApproximatorRadioButtons.add(radioButton);
        }
        fieldsPanel.add(new JLabel("Maximum distance: "), Layout.getLeft(0, i));
        fieldsPanel.add(maximumDistanceTextField, Layout.getLeft(1, i++));
        fieldsPanel.add(new JLabel("Other options: "), Layout.getLeft(0, i));
        fieldsPanel.add(withDistanceCriterionCheckBox, Layout.getLeft(1, i++));
        fieldsPanel.add(alsoExportToJsonCheckBox, Layout.getLeft(1, i));
        add(fieldsPanel, BorderLayout.NORTH);

    }


}
