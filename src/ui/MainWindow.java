package ui;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import core.AbstractApproximatorModule;
import core.TargetDataModel;
import core.state.PointListBasedState;
import geom.Biarc;
import geom.Context2D;
import org.reflections.Reflections;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SpringLayout;
import javax.swing.WindowConstants;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import static core.Configuration.findProperty;
import static core.SourceDataModel.constructFrom;
import static core.TargetDataModel.fromBiarcSequenceAndContext;
import static geom.Context2D.optimalContextFor;
import static java.lang.Boolean.parseBoolean;
import static java.lang.Math.round;
import static multex.Swing.report;

/**
 * User: seickelberg
 * Date: 03.04.13
 */
public class MainWindow extends JFrame implements Observer {

    private final PointListBasedState state;

    private DrawArea drawArea;
    private JLabel statusLabel;
    private Context2D currentContext;

    @Inject
    public MainWindow(@Nonnull PointListBasedState state) {
        this.state = state;
        setTitle("Biarc Approximation");
        setBounds(0, 0, 800, 540);
        addApplicationExitOnWindowClose();
        createMenuIn(this);
        Container frame = getContentPane();
        SpringLayout layout = new SpringLayout();
        frame.setLayout(layout);
        createDrawAreaIn(frame, layout);
        createButtonPanelIn(frame, layout);
        state.addObserver(this);
        setVisible(true);
    }

    public MainWindow(@Nonnull PointListBasedState state, @Nullable String fileName) {
        this(state);
        if (fileName != null) {
            readFileFrom(fileName);
        }
    }

    private void createMenuIn(@Nonnull JFrame frame) {
        JMenuBar menuBar = new JMenuBar();
        menuBar.add(buildFileMenu());
        menuBar.add(buildActionsMenu());
        frame.setJMenuBar(menuBar);
    }

    private JMenu buildActionsMenu() {
        JMenu actionsMenu = new JMenu("Actions");
        JMenuItem approximateMenuItem = new JMenuItem("Approximate");
        approximateMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                state.approximate();
            }
        });
        JMenuItem clearMenuItem = new JMenuItem("Clear");
        clearMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                state.clear();
            }
        });
        JMenuItem optionsMenuItem = new JMenuItem("Options...");
        optionsMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showOptionsDialogAndLoadSelectedModule();
            }
        });
        actionsMenu.add(approximateMenuItem);
        actionsMenu.add(clearMenuItem);
        actionsMenu.add(new JSeparator());
        actionsMenu.add(optionsMenuItem);
        optionsMenuItem.setEnabled(parseBoolean(findProperty("showOptionsDialog")));
        return actionsMenu;
    }

    private JMenu buildFileMenu() {
        JMenu fileMenu = new JMenu("File");
        JMenuItem fileOpenMenuItem = new JMenuItem("Open...");
        final FileActionListener fileActionListener = new FileActionListener();
        fileOpenMenuItem.addActionListener(fileActionListener);
        JMenuItem fileSaveMenuItem = new JMenuItem("Save...");
        fileSaveMenuItem.addActionListener(fileActionListener);
        JMenuItem exitMenuItem = new JMenuItem("Exit");
        exitMenuItem.addActionListener(new ExitActionListener());
        fileMenu.add(fileOpenMenuItem);
        fileMenu.add(fileSaveMenuItem);
        fileMenu.add(new JSeparator());
        fileMenu.add(exitMenuItem);
        return fileMenu;
    }

    private void showOptionsDialogAndLoadSelectedModule() {
        Reflections reflections = new Reflections("core");
        Set<Class<? extends AbstractApproximatorModule>> availableApproximatorModules =
                reflections.getSubTypesOf(AbstractApproximatorModule.class);
        final List<String> moduleNames = new ArrayList<String>();
        for (Class<? extends AbstractApproximatorModule> moduleClass : availableApproximatorModules) {
            moduleNames.add(moduleClass.getName());
        }
        final OptionsDialog dialog = new OptionsDialog(moduleNames, this, state);
        dialog.setModal(true);
        dialog.setVisible(true);
    }

    private void addApplicationExitOnWindowClose() {
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent windowEvent) {
                System.exit(0);
            }
        });
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }

    @Override
    public void update(Observable o, Object arg) {
        drawArea.repaint();
        statusLabel.setText(state.getCurrentStatusText());
    }

    private void createButtonPanelIn(@Nonnull Container frame, @Nonnull SpringLayout layout) {
        StatusBar bp = new StatusBar();
        frame.add(bp);
        layout.putConstraint(SpringLayout.SOUTH, drawArea, -5, SpringLayout.NORTH, bp);
        layout.putConstraint(SpringLayout.WEST, bp, 0, SpringLayout.WEST, frame);
        layout.putConstraint(SpringLayout.EAST, bp, 0, SpringLayout.EAST, frame);
        layout.putConstraint(SpringLayout.SOUTH, bp, -5, SpringLayout.SOUTH, frame);
    }

    private void createDrawAreaIn(@Nonnull Container frame, @Nonnull SpringLayout layout) {
        drawArea = new DrawArea();
        frame.add(drawArea);
        layout.putConstraint(SpringLayout.NORTH, drawArea, 5, SpringLayout.NORTH, frame);
        layout.putConstraint(SpringLayout.WEST, drawArea, 5, SpringLayout.WEST, frame);
        layout.putConstraint(SpringLayout.EAST, drawArea, -5, SpringLayout.EAST, frame);
    }


    private void saveFileTo(@Nonnull String fileName) {
        try {
            if (state.getApproximator() != null && state.getApproximator().getApproximation() != null) {
                //noinspection unchecked
                final List<Biarc> biarcs = (List<Biarc>) state.getApproximator().getApproximation();
                //noinspection unchecked
                currentContext = optimalContextFor(state.getPoints());
                final TargetDataModel model = fromBiarcSequenceAndContext(biarcs, currentContext);
                model.createAndSaveAsIpeDrawing(fileName);
            } else {
                throw new IllegalStateException("No approximator defined or no approximation present!");
            }
        } catch (Throwable t) {
            report(this, t);
        }
    }

    private void readFileFrom(@Nonnull String fileName) {
        try {
            final List<Point2D.Double> pointsRead = constructFrom(fileName).getPointList();
            state.removeAllPoints();
            currentContext = optimalContextFor(pointsRead);
            for (Point2D point : pointsRead) {
                state.addPoint(
                        (int) round(point.getX()),
                        (int) round(point.getY()),
                        currentContext
                );
            }
        } catch (Throwable t) {
            report(this, t);
        }
    }

    private class DrawArea extends JPanel implements MouseListener, MouseMotionListener {
        private static final int pointRadius = 5;
        private boolean mouseActive;
        private int mouseX, mouseY;

        public DrawArea() {
            setBackground(Color.WHITE);
            mouseActive = false;
            mouseX = mouseY = 0;
            addMouseListener(this);
            addMouseMotionListener(this);
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2 = (Graphics2D) g;
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            //noinspection unchecked
            final ImmutableList<Point2D> pointsToDraw = (ImmutableList<Point2D>) state.getPointsAsImmutableList();
            for (Point2D p : pointsToDraw) {
                g2.setColor(Color.BLUE);
                g2.fillOval(
                        (int) round(p.getX()) - pointRadius,
                        (int) round(p.getY()) - pointRadius,
                        2 * pointRadius,
                        2 * pointRadius);
            }
            //noinspection unchecked
            List<Biarc> approximation = (List<Biarc>) state.getApproximator().getApproximation();
            //noinspection ConstantConditions
            if (approximation != null) {
                for (Biarc biarc : approximation) {
                    g2.setColor(Color.BLACK);
                    if (biarc != null) {
                        biarc.drawIn(g2);
                    }

                }
            }
            if (mouseActive) {
                g2.setColor(Color.GRAY);
                g2.fillOval(mouseX - pointRadius, mouseY - pointRadius, 2 * pointRadius, 2 * pointRadius);
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {
            state.addPoint(e.getX(), e.getY());
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            mouseX = e.getX();
            mouseY = e.getY();
            repaint();
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            mouseActive = true;
        }

        @Override
        public void mouseExited(MouseEvent e) {
            mouseActive = false;
            repaint();
        }

        @Override
        public void mouseClicked(MouseEvent e) {
        }

        @Override
        public void mouseReleased(MouseEvent e) {
        }

        @Override
        public void mouseDragged(MouseEvent e) {
        }
    }

    private class StatusBar extends JPanel {
        public StatusBar() {
            setLayout(new FlowLayout(FlowLayout.LEFT, 5, 0));
            statusLabel = new JLabel(state.getCurrentStatusText());
            add(statusLabel);
        }
    }

    private class ExitActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            System.exit(0);
        }
    }

    private class FileActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Ipe file", "ipe", "xml"));
            JMenuItem source = (JMenuItem) actionEvent.getSource();
            if (source.getText().startsWith("Open")) {
                if (fileChooser.showOpenDialog((Component) actionEvent.getSource()) == JFileChooser.APPROVE_OPTION) {
                    readFileFrom(fileChooser.getSelectedFile().getAbsolutePath());
                }
            } else if (source.getText().startsWith("Save")) {
                if (fileChooser.showSaveDialog((Component) actionEvent.getSource()) == JFileChooser.APPROVE_OPTION) {
                    saveFileTo(fileChooser.getSelectedFile().getAbsolutePath());
                }
            }
        }
    }

    public Graphics getDrawAreaGraphics() {
        return drawArea.getGraphics();
    }
}