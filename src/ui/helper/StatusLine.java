package ui.helper;

import java.util.HashMap;

/**
 * User: seickelberg
 * Date: 16.09.13
 */
public class StatusLine extends HashMap<String, String> {

    private static StatusLine singletonInstance;

    private StatusLine() {
        super();
        put("status", "ready");
    }

    public static StatusLine getSingletonInstance() {
        if (singletonInstance == null) {
            singletonInstance = new StatusLine();
        }
        return singletonInstance;
    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder("");
        for (final String key : keySet()) {
            result.append(key);
            result.append(": ");
            result.append(get(key));
            result.append("; ");
        }
        if (result.length() >= 2) {
            result.delete(result.length() - 2, result.length());
        }
        return result.toString();
    }
}
