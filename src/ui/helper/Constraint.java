package ui.helper;

import javax.annotation.Nonnull;

import static java.lang.Double.parseDouble;

/**
 * User: seickelberg
 * Date: 10.09.13
 */
public class Constraint {

    public static void numberRequired(@Nonnull final String value) throws ConstraintViolatedException {
        try {
            parseDouble(value);
        } catch (NumberFormatException e) {
            throw new ConstraintViolatedException("numberRequired", value, e);
        }
    }
}
