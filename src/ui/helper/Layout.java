package ui.helper;

import java.awt.GridBagConstraints;
import java.awt.Insets;

/**
 * User: seickelberg
 * Date: 10.09.13
 */
public class Layout {

    public static GridBagConstraints getLeft(int gridX, int gridY) {
        return getLeftWithWidth(gridX, gridY, 1);
    }

    public static GridBagConstraints getLeftWithWidth(int gridX, int gridY, int widthX) {
        return getConstraint(gridX, gridY, widthX, 1, GridBagConstraints.WEST);
    }

    public static GridBagConstraints getRightWithWidth(int gridX, int gridY, int widthX) {
        return getConstraint(gridX, gridY, widthX, 1, GridBagConstraints.EAST);
    }

    public static GridBagConstraints getLeftStretch(int gridX, int gridY, int withX) {
        GridBagConstraints contraint = getConstraint(gridX, gridY, withX, 1, GridBagConstraints.WEST);
        contraint.weightx = 1.2;
        return contraint;
    }

    public static GridBagConstraints getConstraint(int gridX, int gridY, int widthX, int widthY, int hAlign) {
        return new GridBagConstraints(gridX, gridY, widthX, widthY, 1.0, 0.0, hAlign,
                GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0);
    }
}
