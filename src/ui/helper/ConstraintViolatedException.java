package ui.helper;

import javax.annotation.Nonnull;

/**
 * User: seickelberg
 * Date: 10.09.13
 */
public class ConstraintViolatedException extends Exception {

    public ConstraintViolatedException(@Nonnull final String requirementName, @Nonnull final Object value, Throwable throwable) {
        super("Constraint '" + requirementName + "' violated by '" + value + "'!", throwable);
    }
}
