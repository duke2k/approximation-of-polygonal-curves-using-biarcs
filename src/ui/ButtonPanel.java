package ui;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.FlowLayout;

/**
 * User: seickelberg
 * Date: 10.09.13
 */
public class ButtonPanel extends JPanel {

    public ButtonPanel() {
        super();
        this.setBorder(new EmptyBorder(5, 5, 0, 0));
        this.setLayout(new FlowLayout(FlowLayout.RIGHT, 0, 0));
    }
}
