package annotations;

import static annotations.Pattern.Type.undefined;

/**
 * User: Duke2k
 * Date: 04.01.14
 */
public @interface Pattern {
    Type name() default undefined;

    public enum Type {
        singleton,
        composition,
        undefined
    }
}
