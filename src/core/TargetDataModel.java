package core;

import annotations.Pattern;
import com.fasterxml.jackson.databind.ObjectMapper;
import core.jaxb.generated.Ipe;
import core.jaxb.generated.Page;
import core.jaxb.generated.Path;
import geom.Biarc;
import geom.CircularArc;
import geom.Context2D;
import io.IpeFileWriter;
import multex.Exc;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static annotations.Pattern.Type.composition;
import static com.google.inject.internal.util.$Preconditions.checkArgument;
import static core.Configuration.findProperty;
import static core.Utils.*;
import static io.Utils.*;
import static java.lang.Boolean.parseBoolean;
import static javax.xml.bind.JAXB.marshal;
import static javax.xml.bind.JAXB.unmarshal;

/**
 * User: seickelberg
 * Date: 15.04.13
 */
@Pattern(name = composition)
public class TargetDataModel {

    private List<Biarc> biarcList;
    private Context2D context;
    private boolean alsoExportToJson = parseBoolean(findProperty("alsoExportToJson"));

    private TargetDataModel() {
    }

    @Nonnull
    public static TargetDataModel fromBiarcSequenceAndContext(List<Biarc> biarcList, @Nullable Context2D context) {
        TargetDataModel result = new TargetDataModel();
        result.setBiarcList(biarcList);
        result.setContext(context == null ? new Context2D() : context);
        return result;
    }

    public void createAndSaveAsIpeDrawing(@Nonnull String fileName) {
        final String absoluteTemplatePath = new File(TEMPLATE_PATH).getAbsolutePath();
        final InputStream templateInputStream = TargetDataModel.class.getResourceAsStream("/" + TEMPLATE_PATH);
        final Ipe ipeDrawing;
        if (templateInputStream == null) {
            ipeDrawing = unmarshal(absoluteTemplatePath, Ipe.class);
        } else {
            ipeDrawing = unmarshal(templateInputStream, Ipe.class);
        }
        final List<Biarc> revertedBiarcs = context.getRevertedBiarcList(biarcList);
        final List<CircularArc> circularArcs = new ArrayList<CircularArc>(biarcList.size() * 2);
        for (Biarc biarc : revertedBiarcs) {
            if (biarc != null) {
                circularArcs.add(biarc.getFirstArc());
                circularArcs.add(biarc.getSecondArc());
            }
        }
        final Page firstPathablePage = findFirstPathablePage(ipeDrawing);
        assert firstPathablePage != null;
        removeEmptyPathsFrom(firstPathablePage);
        for (CircularArc arc : circularArcs) {
            Path path = new Path(); // every arc is placed in its own <path> tag in Ipe
            path.setLayer("alpha");
            path.setStroke("darkgreen"); // this could be configurable later on...
            path.setContent(arc.getIpeRepresentationBy(context));
            firstPathablePage.getGroupOrImageOrUse().add(path);
        }
        setModifiedIn(ipeDrawing, new Date());
        Writer writer = null;
        try {
            writer = new IpeFileWriter(fileName);
            marshal(ipeDrawing, writer);
        } catch (Exception e) {
            throw new Exc("Could not create and save file with name {0} from target data model.", e, fileName);
        } finally {
            closeQuietly(writer);
        }
        checkFileNameAndFormat(fileName); // check and validate the completed Ipe file
        if (alsoExportToJson) {
            exportToJson(ipeDrawing, fileName + ".json");
        }
    }

    private void exportToJson(@Nonnull Ipe ipeDrawing, @Nonnull String fileName) {
        final ObjectMapper mapper = new ObjectMapper();
        final File jsonFile = new File(fileName);
        try {
            mapper.writeValue(jsonFile, ipeDrawing);
        } catch (IOException e) {
            throw new Exc("Could not create and save json file with name {0} from target data model.", e, fileName);
        }
    }

    private void setModifiedIn(@Nonnull Ipe drawing, @Nonnull Date date) {
        checkArgument(drawing.getInfo() != null);
        SimpleDateFormat format = new SimpleDateFormat(IPE_DATE_FORMAT);
        //noinspection ConstantConditions
        drawing.getInfo().setModified("D:" + format.format(date));
    }

    public void setBiarcList(List<Biarc> biarcList) {
        this.biarcList = biarcList;
    }

    public void setContext(@Nonnull Context2D context) {
        this.context = context;
    }
}
