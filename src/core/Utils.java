package core;

import core.jaxb.generated.Ipe;
import core.jaxb.generated.Page;
import core.jaxb.generated.Path;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

/**
 * User: seickelberg
 * Date: 16.04.13
 */
public class Utils {

    public static final String IPE_DATE_FORMAT = "yyyyMMddHHmmss";
    public static final int IPE_DATE_PARSE_START = 2;

    @Nullable
    public static Path findFirstPathIn(@Nonnull Ipe drawing) {
        for (Page page : drawing.getPage()) {
            for (Object object : page.getGroupOrImageOrUse()) {
                if (object instanceof Path) {
                    return (Path) object;
                }
            }
        }
        return null;
    }

    @Nullable
    public static Page findFirstPathablePage(@Nonnull Ipe drawing) {
        for (Page page : drawing.getPage()) {
            for (Object object : page.getGroupOrImageOrUse()) {
                if (object instanceof Path) {
                    return page;
                }
            }
        }
        return null;
    }

    public static void removeEmptyPathsFrom(@Nonnull Page page) {
        // this is to avoid a ConcurrentModificationException being thrown
        final List<Object> deepCopyOfPageChildren = new ArrayList<Object>(page.getGroupOrImageOrUse().size());
        for (Object object : page.getGroupOrImageOrUse()) {
            deepCopyOfPageChildren.add(object);
        }
        for (Object object : deepCopyOfPageChildren) {
            if (object instanceof Path) {
                final String content = ((Path) object).getContent();
                if (content == null || content.isEmpty()) {
                    page.getGroupOrImageOrUse().remove(object);
                }
            }
        }
    }

    @Nonnull
    public static List<Path> findAllPathsIn(@Nonnull Ipe drawing) {
        final List<Path> result = new ArrayList<Path>();
        for (Page page : drawing.getPage()) {
            for (Object object : page.getGroupOrImageOrUse()) {
                if (object instanceof Path) {
                    result.add((Path) object);
                }
            }
        }
        return result;
    }
}
