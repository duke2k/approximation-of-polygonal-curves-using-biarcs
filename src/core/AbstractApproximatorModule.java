package core;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;

import java.util.ArrayList;
import java.util.List;

import static java.awt.geom.Point2D.Double;

/**
 * User: seickelberg
 * Date: 27.05.13
 */
public abstract class AbstractApproximatorModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(new TypeLiteral<List<Double>>() {
        }).to(new TypeLiteral<ArrayList<Double>>() {
        });
    }
}
