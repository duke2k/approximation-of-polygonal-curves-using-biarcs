package core;

import annotations.Pattern;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Properties;

import static annotations.Pattern.Type.singleton;
import static io.Utils.loadProperties;

/**
 * User: seickelberg
 * Date: 10.09.13
 * <p/>
 * This class is a singleton instance to provide current, centralized configuration details, initialized with
 * the contents of config.properties.
 */
@Pattern(name = singleton)
public class Configuration {

    private static Configuration singleton;
    private final HashMap<String, String> propertiesMap = new HashMap<String, String>();

    private Configuration() {
        final Properties properties = new Properties();
        loadProperties(properties);
        for (final String name : properties.stringPropertyNames()) {
            propertiesMap.put(name, properties.getProperty(name));
        }
    }

    @Nonnull
    public static Configuration getInstance() {
        if (singleton == null) {
            singleton = new Configuration();
        }
        return singleton;
    }

    @Nullable
    public String getProperty(@Nonnull final String name) {
        return propertiesMap.get(name);
    }

    @Nullable
    public static String findProperty(@Nonnull final String name) {
        return getInstance().getProperty(name);
    }

    public void set(@Nonnull final String name, @Nonnull final String value) {
        propertiesMap.put(name, value);
    }
}
