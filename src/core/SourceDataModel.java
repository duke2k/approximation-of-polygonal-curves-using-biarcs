package core;

import annotations.Pattern;
import com.google.inject.Inject;
import core.jaxb.generated.Ipe;
import core.jaxb.generated.Path;
import geom.Context2D;
import io.IpeFileReader;
import multex.Exc;

import javax.annotation.Nonnull;
import java.awt.geom.Point2D;
import java.io.*;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import static annotations.Pattern.Type.composition;
import static com.google.inject.internal.util.$Preconditions.checkArgument;
import static core.Utils.*;
import static geom.Context2D.optimalContextFor;
import static io.Utils.closeQuietly;
import static javax.xml.bind.JAXB.unmarshal;

/**
 * User: seickelberg
 * Date: 15.04.13
 */
@Pattern(name = composition)
public class SourceDataModel {

    public static final String DTD_STRING = "<!DOCTYPE ipe SYSTEM \"ipe.dtd\">";

    private final List<Point2D.Double> pointList;
    private final Context2D context;
    private final Date modified;

    @Inject
    private SourceDataModel(List<Point2D.Double> pointList, Date modified) {
        this.modified = modified;
        context = optimalContextFor(pointList);
        this.pointList = apply(context, pointList);
    }

    @Nonnull
    private List<Point2D.Double> apply(@Nonnull Context2D context, @Nonnull List<Point2D.Double> pointList) {
        final List<Point2D.Double> result = new ArrayList<Point2D.Double>(pointList.size());
        for (Point2D.Double point : pointList) {
            result.add(new Point2D.Double(
                    point.getX() - context.getxOffset(),
                    (point.getY() - context.getyMaxValue()) * -1 - context.getyOffset()
            ));
        }
        context.setReadFromIpeFile(true);
        return result;
    }

    public static SourceDataModel constructFrom(@Nonnull String fileName) {
        Reader reader = null;
        final List<Point2D.Double> pointsForResult;
        Date modified = new Date(); // now
        try {
            removeDoctypeDTDReference(fileName);
            reader = new IpeFileReader(fileName);
            final Ipe ipeDrawing = unmarshal(reader, Ipe.class);
            final String content;
            final Path contentPath = findFirstPathIn(ipeDrawing);
            if (contentPath != null) {
                content = contentPath.getContent();
            } else {
                throw new Exc("The source file with name {0} does not contain any paths.", null, fileName);
            }
            pointsForResult = findPointsIn(content);
            modified = retrieveModifiedDateFrom(ipeDrawing);
        } catch (Exception e) {
            throw new Exc("Could not construct source data model from file {0}.", e, fileName);
        } finally {
            closeQuietly(reader);
        }
        return new SourceDataModel(pointsForResult, modified);
    }

    private static void removeDoctypeDTDReference(String fileName) throws IOException {
        final BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
        final StringBuilder targetXml = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            if (!line.contains(DTD_STRING)) {
                targetXml.append(line);
                targetXml.append("\n");
            }
        }
        closeQuietly(bufferedReader);
        final BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName, false));
        bufferedWriter.append(targetXml.toString());
        bufferedWriter.flush();
        closeQuietly(bufferedWriter);
    }

    @Nonnull
    private static Date retrieveModifiedDateFrom(@Nonnull Ipe drawing) {
        Date result = new Date(); // now
        checkArgument(drawing.getInfo() != null);
        //noinspection ConstantConditions
        final String modifiedString = drawing.getInfo().getModified();
        if (modifiedString != null) {
            SimpleDateFormat format = new SimpleDateFormat(IPE_DATE_FORMAT);
            result = format.parse(modifiedString, new ParsePosition(IPE_DATE_PARSE_START));
        }
        return result;
    }

    @Nonnull
    private static List<Point2D.Double> findPointsIn(@Nonnull String content) {

        List<Point2D.Double> result = new ArrayList<Point2D.Double>();
        StringTokenizer tokenizer = new StringTokenizer(content);

        double[] tempPoint = new double[2];
        int tempPointPos = 0;

        while (tokenizer.hasMoreTokens()) {
            String token = tokenizer.nextToken();

            try {

                tempPoint[tempPointPos++] = Double.parseDouble(token);
            } catch (NumberFormatException ignored) {
                tempPointPos = 0;
                result.add(new Point2D.Double(tempPoint[0], tempPoint[1]));

                if (token.equalsIgnoreCase("h")) {
                    break;
                } else if (!token.equalsIgnoreCase("m") && !token.equalsIgnoreCase("l")) {
                    throw new Exc("Elements other than moveTo or lineTo are not yet supported.");
                }
            }
        }
        return result;
    }

    public List<Point2D.Double> getPointList() {
        return pointList;
    }

    @Nonnull
    public Date getModified() {
        return modified;
    }

    @Nonnull
    public Context2D getContext() {
        return context;
    }
}
