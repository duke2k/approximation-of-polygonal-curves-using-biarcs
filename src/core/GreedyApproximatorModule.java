package core;

import core.algorithm.BiarcApproximator;
import core.algorithm.GreedyApproximator;
import core.state.BiarcListResultState;
import core.state.State;

/**
 * User: Sven
 * Date: 06.06.13
 */
public class GreedyApproximatorModule extends AbstractApproximatorModule {

    protected void configure() {
        super.configure();
        bind(State.class).to(BiarcListResultState.class);
        bind(BiarcApproximator.class).to(GreedyApproximator.class);
    }
}
