package core.algorithm;

import com.google.inject.Inject;
import geom.Biarc;
import geom.CircularArc;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.awt.geom.Point2D;
import java.util.List;

/**
 * User: Samuel
 * Date: 08.05.13
 */
public abstract class BiarcApproximator implements Approximator<List<Point2D.Double>, List<Biarc>> {

    public static final double ALLOWED_TOLERANCE_FOR_APPROXIMATION = 50.0; // px

    protected List<Biarc> approximation;

    protected double maxDistance = ALLOWED_TOLERANCE_FOR_APPROXIMATION;

    @Inject
    public BiarcApproximator() {
    }

    protected void matchJoinDirectionsOf(@Nonnull CircularArc firstArc, @Nonnull CircularArc secondArc) {
        final double firstAngle = firstArc.getEndDirection();
        final double secondAngle = secondArc.getStartDirection();
        final double averageAngle = (firstAngle + secondAngle) / 2.0;
        firstArc.setEndDirection(averageAngle);
        secondArc.setStartDirection(averageAngle);
    }

    protected boolean isSufficientApproximationOf(@Nonnull List<Point2D> points, @Nonnull Biarc biarc) {
        boolean result = true;
        for (Point2D point : points) {
            if (biarc.distanceTo(point) > maxDistance) {
                result = false;
                break;
            }
        }
        return result;
    }

    @Nonnull
    public List<Point2D.Double> getConvexHullOf(@Nonnull List<Point2D.Double> points) {
        final FastConvexHull convexHull = new FastConvexHull();
        return convexHull.compute(points);
    }

    @Override
    public void clear() {
        if (approximation != null) {
            approximation.clear();
        }
    }

    @Nullable
    @Override
    public List<Biarc> getApproximation() {
        return approximation;
    }

    @Nonnull
    @Override
    public String getStatusInfoText() {
        return "using " + (approximation != null ? approximation.size() : 0) + " biarcs";
    }

    @Override
    public void setMaxDistance(@Nonnegative double maxDistance) {
        this.maxDistance = maxDistance;
    }
}
