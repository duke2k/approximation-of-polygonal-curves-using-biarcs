package core.algorithm;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * User: Samuel
 * Date: 08.05.13
 */
public interface Approximator<I, O> {

    public void executeWith(@Nonnull I source);

    public void clear();

    @Nullable
    public O getApproximation();

    @Nonnull
    public String getStatusInfoText();

    void setMaxDistance(@Nonnegative double maxDistance);
}
