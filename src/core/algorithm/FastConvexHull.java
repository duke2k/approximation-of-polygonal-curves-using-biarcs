package core.algorithm;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static java.util.Arrays.asList;

/**
 * User: Samuel
 * Date: 22.05.13
 * Source: https://code.google.com/p/convex-hull/source/browse/Convex+Hull/src/algorithms/FastConvexHull.java
 * <p/>
 * The original class definition has been slightly modified to suit our needs.
 */
public class FastConvexHull {

    public List<Point2D.Double> compute(List<Point2D.Double> points) {
        List<Point2D.Double> xSorted = deepCopy(points);
        Collections.sort(xSorted, new XCompare());
        int n = xSorted.size();
        Point2D.Double[] lUpper = new Point2D.Double[n];
        lUpper[0] = xSorted.get(0);
        lUpper[1] = xSorted.get(1);
        int lUpperSize = 2;
        for (int i = 2; i < n; i++) {
            lUpper[lUpperSize] = xSorted.get(i);
            lUpperSize++;
            while (lUpperSize > 2 && !rightTurn(lUpper[lUpperSize - 3], lUpper[lUpperSize - 2], lUpper[lUpperSize - 1])) {
                // Remove the middle point of the three last
                lUpper[lUpperSize - 2] = lUpper[lUpperSize - 1];
                lUpperSize--;
            }
        }
        Point2D.Double[] lLower = new Point2D.Double[n];
        lLower[0] = xSorted.get(n - 1);
        lLower[1] = xSorted.get(n - 2);
        int lLowerSize = 2;
        for (int i = n - 3; i >= 0; i--) {
            lLower[lLowerSize] = xSorted.get(i);
            lLowerSize++;
            while (lLowerSize > 2 && !rightTurn(lLower[lLowerSize - 3], lLower[lLowerSize - 2], lLower[lLowerSize - 1])) {
                // Remove the middle point of the three last
                lLower[lLowerSize - 2] = lLower[lLowerSize - 1];
                lLowerSize--;
            }
        }

        List<Point2D.Double> result = new ArrayList<Point2D.Double>();
        result.addAll(asList(lUpper).subList(0, lUpperSize));
        result.addAll(asList(lLower).subList(1, lLowerSize - 1));
        return result;
    }

    private List<Point2D.Double> deepCopy(List<Point2D.Double> points) {
        ArrayList<Point2D.Double> xSorted = new ArrayList<Point2D.Double>(points.size());
        for (Point2D.Double point : points) {
            xSorted.add(new Point2D.Double(point.getX(), point.getY()));
        }
        return xSorted;
    }

    private boolean rightTurn(Point2D a, Point2D b, Point2D c) {
        return (b.getX() - a.getX()) * (c.getY() - a.getY()) - (b.getY() - a.getY()) * (c.getX() - a.getX()) > 0;
    }

    private class XCompare implements Comparator<Point2D> {
        @Override
        public int compare(Point2D o1, Point2D o2) {
            return (new Double(o1.getX())).compareTo(o2.getX());
        }
    }
}
