package core.algorithm;

import geom.Biarc;
import geom.CircularArc;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides a static method to compute a local optimal biarc for a given polygon and start and end tangent vector.
 * <p/>
 * Also provides a static method to compute the maximum Distance between a polygon and a biarc.
 * User: Sven
 * Date: 05.06.13
 */
public class BiarcUtils {

    /**
     * Computes Distance between the polygon and the biarc. Distance being the maximum auf all distances from all points
     * to the biarc.
     */
    public static double getDistance(@Nonnull List<Point2D> polygon, @Nullable Biarc biarc) {
        double distance = 0;
        double temp;
        for (Point2D pj : polygon) {
            if (pj != null && biarc != null) {
                temp = biarc.distanceTo(pj);
                if (temp > distance) {
                    distance = temp;
                }
            }
        }
        return distance;
    }

    /**
     * This method computes the biarc, that has approximately the minimum distance to the given polygon.
     * tstart and tend are the tangent vectors of the first and last points of the polygon.
     * If a special case occurs, where four arcs would be needed, the method returns null.
     * An iterative approach is being used.
     *
     * @param polygon Liste mit allen Punkten des Abschnitts
     * @param tstart  Richtungsvektor am ersten Punkt, normalisiert, d.h. ||tstart|| = 1
     * @param tend    Richtungsvektor am letzten Punkt, normalisiert, d.h. ||tstart|| = 1
     */
    public static Biarc getOptBiarc(List<Point2D> polygon, Point2D.Double tstart, Point2D.Double tend) {
        Point2D pStart, pEnd;
        double specialCase;

        //for debugging
        assert polygon.size() > 1;

        pStart = polygon.get(0);
        pEnd = polygon.get(polygon.size() - 1);

        //return null if special case which requires four arcs
        specialCase = pStart.getX() * pEnd.getX() + pStart.getY() * pEnd.getY();
        if (specialCase == 1) return null;

        //main loop compute best Biarc
        int d = 200;
        double p = 0.25;
        double rupper = 1000000000;
        double rcenter = 1;
        double rlower = 1 / rupper;
        double minDist = Double.MAX_VALUE;
        int minIndex;
        double minRatio = 0;
        ArrayList<Double> ratios;
        while (true) {
            double temp;
            Biarc tempBiarc;
            minIndex = -1;
            ratios = new ArrayList<Double>();

            for (int i = 0; i < 2 * d + 1; i++) {
                //check in which way to compute the current ratio and do so
                if (i <= d)
                    ratios.add(computeRi(i, d, rlower, rcenter));
                else
                    ratios.add(computeRdplusi((i - d), d, rcenter, rupper));
            }

            //try 2*d+1 different values in the current range and find minimum
            for (int i = 0; i < ratios.size(); i++) {

                //compute current biarc
                try {
                    tempBiarc = getBiarc(pStart, pEnd, tstart, tend, ratios.get(i));
                } catch (IllegalArgumentException ex) {
                    continue;
                }

                //do we have a biarc with the current ratio? compute distance : try next ratio
                if (tempBiarc == null)
                    continue;
                else
                    temp = getDistance(polygon, tempBiarc);

                //do we have a new minimum?
                if (temp <= minDist) {
                    minDist = temp;
                    minIndex = i;
                    minRatio = ratios.get(i);
                }
            }

            //if the range around the minimum is small enough, we are finished
            if (rupper - rlower < Math.pow(10, -4)) break;

            if (minIndex == -1) return null;
            assert minRatio != 0;
            //assert minIndex != -1;

            //compute new range around the minimum
            if (minIndex == 0) {
                rlower = ratios.get(0);
                rcenter = ratios.get(1);
                rupper = ratios.get(2);
            } else if (minIndex == 2 * d) {
                rlower = ratios.get(ratios.size() - 3);
                rcenter = ratios.get(ratios.size() - 2);
                rupper = ratios.get(ratios.size() - 1);
            } else {
                rlower = ratios.get(minIndex - 1);
                rcenter = ratios.get(minIndex);
                rupper = ratios.get(minIndex + 1);
            }

            //reduce d with further iterations
            d = Math.max(2, (int) (1 - p) * d);

            //for debugging
            //System.out.println("***** "+rlower + "\n***** "+rcenter+ "\n***** " +rupper);

        }

        return getBiarc(pStart, pEnd, tstart, tend, ratios.get(minIndex));
    }

    /**
     * This method computes a specific biarc which is uniquely given by two points, two tangent vectors
     * and the ratio.
     * If there occurs the second special case null will be returned. (Four arcs would be needed in this case)
     */
    private static Biarc getBiarc(Point2D start, Point2D end, Point2D.Double tstart, Point2D.Double tend, double ratio) {
        double p0_x = start.getX();
        double p0_y = start.getY();
        double p4_x = end.getX();
        double p4_y = end.getY();
        double tsx;
        double tsy;
        double tex;
        double tey;

        tsx = tstart.getX();
        tsy = -tstart.getY();
        tex = tend.getX();
        tey = -tend.getY();
        double beta1, beta2, beta;
        double alpha;
        double p1_x, p1_y, p2_x, p2_y, p3_x, p3_y;

        double vx = p0_x - p4_x;
        double vy = p0_y - p4_y;

        //return null if second special case where four arcs are needed
        double specialCase = vx * (ratio * tsx + tex) + vy * (ratio * tsy + tey);
        if (specialCase == 0) return null;

        //solve equation (3) with specific ratio r
        beta1 = (-Math.sqrt(Math.pow((2 * ratio * tsx * vx + 2 * ratio * tsy * vy + 2 * tex * vx + 2 * tey * vy), 2) -
                4 * (Math.pow(vx, 2) + Math.pow(vy, 2)) * (2 * ratio * tex * tsx + 2 * ratio * tey * tsy - 2 * ratio)) -
                2 * ratio * tsx * vx - 2 * ratio * tsy * vy - 2 * tex * vx - 2 * tey * vy) /
                (2 * (2 * ratio * tex * tsx + 2 * ratio * tey * tsy - 2 * ratio));
        beta2 = (-2 * tex * vx - 2 * ratio * tsx * vx - 2 * tey * vy - 2 * ratio * tsy * vy +
                Math.sqrt(Math.pow((2 * tex * vx + 2 * ratio * tsx * vx + 2 * tey * vy + 2 * ratio * tsy * vy), 2) -
                        4 * (-2 * ratio + 2 * ratio * tex * tsx + 2 * ratio * tey * tsy) * (Math.pow(vx, 2) + Math.pow(vy, 2)))) /
                (2 * (-2 * ratio + 2 * ratio * tex * tsx + 2 * ratio * tey * tsy));

        beta = Math.max(beta1, beta2);
        if (beta1 > 0 && beta2 > 0) {
            //System.out.println("Two Solutions!");
            beta = Math.min(beta1, beta2);
        }

        //if beta is still negative no biarc is possible with this ratio
        if (beta <= 0) return null;

        //solve equation (2) with beta
        alpha = (-2 * beta * tex * vx - Math.pow(vx, 2) - 2 * beta * tey * vy - Math.pow(vy, 2)) /
                (2 * (-beta + beta * tex * tsx + beta * tey * tsy + tsx * vx + tsy * vy));

        //if alpha is negative no biarc is possible with this ratio
        if (alpha <= 0) return null;

        //compute p1, p2, p3
        p1_x = p0_x + alpha * tsx;
        p1_y = p0_y + alpha * (tsy);

        p3_x = p4_x - beta * tex;
        p3_y = p4_y - beta * (tey);

        p2_x = (beta / (alpha + beta)) * p1_x + (alpha / (alpha + beta)) * p3_x;
        p2_y = (beta / (alpha + beta)) * p1_y + (alpha / (alpha + beta)) * p3_y;

        //only computable solutions
        if (Double.isNaN(p0_x) || Double.isNaN(p0_y) || Double.isNaN(p1_x) || Double.isNaN(p1_y) || Double.isNaN(p2_x)
                || Double.isNaN(p2_y) || Double.isNaN(p3_x) || Double.isNaN(p3_y) || Double.isNaN(p4_x) ||
                Double.isNaN(p4_y) || Double.isInfinite(p0_x) || Double.isInfinite(p0_y) || Double.isInfinite(p1_x) ||
                Double.isInfinite(p1_y) || Double.isInfinite(p2_x) || Double.isInfinite(p2_y) ||
                Double.isInfinite(p3_x) || Double.isInfinite(p3_y) || Double.isInfinite(p4_x) ||
                Double.isInfinite(p4_y)) return null;
        return createBiarcFrom5Points(p0_x, p0_y, p1_x, p1_y, p2_x, p2_y, p3_x, p3_y, p4_x, p4_y);
    }

    /**
     * Diese Funktion spiegelt den Punkt an der Geraden, wird doch nicht benötigt, aber sie ist sehr schön geworden :)
     */
    private static Point2D.Double mirrorPointAtLine(Line2D.Double axis, double px, double py) {
        double psx = axis.getX1(), psy = axis.getY1(), pex = axis.getX2(), pey = axis.getY2();

        //Lotfußpunkt berechnen
        double s = (psx / (pex - psx) - psy / (pey - psy) - px / (pex - psx) + py / (pey - psy)) /
                (pey / (pex - psx) + pex / (pey - psy) - psx / (pey - psy) -
                        psy / (pex - psx));

        double lotf_x = px + (pey - psy) * s;
        double lotf_y = py + (-pex + psx) * s;

        //Spiegelpunkt berechnen
        double x = px + 2 * (lotf_x - px);
        double y = py + 2 * (lotf_y - py);

        return new Point2D.Double(x, y);

    }

    /**
     * This method creates the unique biarc, that is given by the five points
     * TODO: p3 is never used! Do we really need it?
     */
    private static Biarc createBiarcFrom5Points(double p0_x, double p0_y, double p1_x, double p1_y, double p2_x,
                                                double p2_y, double p3_x, double p3_y, double p4_x, double p4_y) {
        Point2D.Double start = new Point2D.Double(p0_x, p0_y);
        Point2D.Double end = new Point2D.Double(p2_x, p2_y);
        CircularArc first = new CircularArc(start, end, new Point2D.Double((p1_x - p0_x), (p1_y - p0_y)));

        start = new Point2D.Double(p2_x, p2_y);
        end = new Point2D.Double(p4_x, p4_y);
        CircularArc second = new CircularArc(start, end, first.getEndDirection());

        return new Biarc(first, second);
    }

    /*
        Computes the Ratio for Index i if i <= d
     */
    private static double computeRi(int i, int d, double rl, double rc) {
        double temp;
        assert rl < rc;
        if (0 < rl && rl < rc && rc <= 1) {
            temp = 1 / ((d - i) / (d * rl) + i / (d * rc));
        } else {
            temp = (((d - i) * rl) / d) + ((i * rc) / d);
        }
        return temp;
    }

    /*
        Computes the Ratio for Index i if d <= i
     */
    private static double computeRdplusi(int i, int d, double rc, double ru) {
        double temp;
        assert rc < ru;
        if (0 < rc && rc < ru && ru <= 1) {
            temp = 1 / ((d - i) / (d * rc) + i / (d * ru));
        } else {
            temp = (((d - i) * rc) / d) + ((i * ru) / d);
        }
        return temp;
    }
}