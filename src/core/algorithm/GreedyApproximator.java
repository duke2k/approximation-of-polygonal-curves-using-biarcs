package core.algorithm;

import geom.Biarc;

import javax.annotation.Nonnull;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import static core.Configuration.findProperty;
import static java.lang.Boolean.parseBoolean;

/**
 * Dieser Greedy-Ansatz beginnt am ersten Punkt und nimmt so lange Punkte in die temporäre Liste für den nächsten
 * Biarc auf wie die Krümmung das gleiche Vorzeichen behält. Danach werden noch einmal so lange Punkte für die
 * geänderte Krümmung aufgenommen bis diese sich wieder ändert.
 * Dann wird ein Biarc berechnet und in die Liste aufgenommen.
 * Danach wir der nächste Biarc berechnet bis alle Punkte verarbeitet sind.
 * User: Sven
 * Date: 06.06.13
 */
public class GreedyApproximator extends BiarcApproximator {
    @Override
    public void executeWith(@Nonnull List<Point2D.Double> source) {
        //activate optimization
        boolean withDistanceCriterion = parseBoolean(findProperty("withDistanceCriterion"));

        //input in ArrayList packen
        ArrayList<Point2D> input = new ArrayList<Point2D>(source);

        //Ergebnisliste
        approximation = new ArrayList<Biarc>();

                            /* main loop variablen */
        //exit-variablen für die inneren while-Schleifen
        boolean firstReady = false, secondReady = false, looping = false;

        //Variablen zum speichern der Krümmungsrichtung durch +1, 0, -1; die Zahl 3 verwende ich als undef.
        int firstCurvature = 3, secondCurvature = 3, curCurvature;

        //pointer auf den Input
        int i = 0;

        //Liste der Punkte für den aktuellen Biarc
        ArrayList<Point2D> currentBiarc = new ArrayList<Point2D>();

        //Punkte, die zur Berechnung der Start- und Endrichtungen für den currentBiarc benötigt werden
        Point2D curPoint, firstExtremePoint, middlePoint;

        //temporäre Variablen
        Biarc biarc;
        double curDirection, startDirection, midSigX;

            /* main loop*/
        while (i < input.size() - 1) {

            //die ersten zwei Punkte hinzufügen, sofern genug vorhanden
            if (i + 1 <= input.size() - 1) {
                currentBiarc.add(input.get(i));
                i++;
                currentBiarc.add(input.get(i));
                i++;
                //Mittelpunkt initialisieren (Mitte zwischen den beiden Punkten)
                middlePoint = new Point2D.Double(
                        (currentBiarc.get(0).getX() + 0.5 * (currentBiarc.get(1).getX() - currentBiarc.get(0).getX())),
                        (currentBiarc.get(0).getY() + 0.5 * (-(currentBiarc.get(1).getY() - currentBiarc.get(0).getY()))));
                //den Punkt zur berechnung der Startrichtung initialisieren
                firstExtremePoint = currentBiarc.get(1);
            } else {
                //to do
                break;
            }

            //Punkte für ersten Arc sammeln
            while (!firstReady && i < input.size() - 1) {
                curPoint = input.get(i);
                currentBiarc.add(curPoint);

                //check curvature of graph
                curCurvature = getCurvature(currentBiarc.get(currentBiarc.size() - 3),
                        currentBiarc.get(currentBiarc.size() - 2), currentBiarc.get(currentBiarc.size() - 1));

                if (firstCurvature == 3)
                    firstCurvature = curCurvature;
                else if (firstCurvature != curCurvature) {
                    secondCurvature = curCurvature;
                    firstReady = true;
                    middlePoint = currentBiarc.get(currentBiarc.size() - 2);
                }

                //save extreme point to compute start direction later
                if (firstCurvature == +1) {
                    if (firstExtremePoint == null)
                        firstExtremePoint = curPoint;
                    else if (
                            (-(curPoint.getY() - currentBiarc.get(0).getY())) / (curPoint.getX() - currentBiarc.get(0).getX()) >
                                    (-(firstExtremePoint.getY() - currentBiarc.get(0).getY())) / (firstExtremePoint.getX() -
                                            currentBiarc.get(0).getX())
                            )
                        firstExtremePoint = curPoint;


                } else if (firstCurvature == -1) {
                    if (firstExtremePoint == null)
                        firstExtremePoint = curPoint;
                    else if (
                            (-(curPoint.getY() - currentBiarc.get(0).getY())) / (curPoint.getX() - currentBiarc.get(0).getX()) <
                                    (-(firstExtremePoint.getY() - currentBiarc.get(0).getY())) / (firstExtremePoint.getX() -
                                            currentBiarc.get(0).getX())
                            )
                        firstExtremePoint = curPoint;
                }

                //loop detection
                startDirection = (-(firstExtremePoint.getY() - currentBiarc.get(0).getY())) / (firstExtremePoint.getX() -
                        currentBiarc.get(0).getX());
                curDirection = (-(currentBiarc.get(currentBiarc.size() - 1).getY() -
                        currentBiarc.get(currentBiarc.size() - 2).getY())) / (currentBiarc.get(currentBiarc.size() - 1).getX()
                        - currentBiarc.get(currentBiarc.size() - 2).getX());
                if (startDirection < 0) {
                    if (curDirection > -startDirection || Math.signum(firstExtremePoint.getX() -
                            currentBiarc.get(0).getX()) != Math.signum(currentBiarc.get(currentBiarc.size() - 1).getX()
                            - currentBiarc.get(currentBiarc.size() - 2).getX())) {
                        looping = true;
                        i++;
                        break;
                    }
                } else {
                    if (curDirection < -startDirection || Math.signum(firstExtremePoint.getX() -
                            currentBiarc.get(0).getX()) != Math.signum(currentBiarc.get(currentBiarc.size() - 1).getX()
                            - currentBiarc.get(currentBiarc.size() - 2).getX())) {
                        looping = true;
                        i++;
                        break;
                    }
                }

                i++;
            }

            //Steigung am Jointpunkt für loop-detection beim zweiten Sammeln ermitteln
            curPoint = peekDirectionAhead(input, i);
            midSigX = Math.signum(curPoint.getX());
            startDirection = (-(curPoint.getY())) / (curPoint.getX());

            //Punkte für zweiten Arc sammeln
            while (!secondReady && i < input.size() - 1 && !looping) {
                curPoint = input.get(i);
                currentBiarc.add(curPoint);

                //check curvature of graph
                curCurvature = getCurvature(currentBiarc.get(currentBiarc.size() - 3),
                        currentBiarc.get(currentBiarc.size() - 2), currentBiarc.get(currentBiarc.size() - 1));

                if (secondCurvature != curCurvature) {
                    secondReady = true;
                    currentBiarc.remove(currentBiarc.size() - 1);
                    i--;
                }

                //loop detection
                curDirection = (-(currentBiarc.get(currentBiarc.size() - 1).getY() -
                        currentBiarc.get(currentBiarc.size() - 2).getY())) / (currentBiarc.get(currentBiarc.size() - 1).getX()
                        - currentBiarc.get(currentBiarc.size() - 2).getX());
                if (startDirection < 0)
                    if (curDirection > -startDirection || Math.signum(currentBiarc.get(currentBiarc.size() - 1).getX()
                            - currentBiarc.get(currentBiarc.size() - 2).getX()) != midSigX) {
                        i++;
                        break;
                    } else if (curDirection < -startDirection || Math.signum(currentBiarc.get(currentBiarc.size() - 1).getX()
                            - currentBiarc.get(currentBiarc.size() - 2).getX()) != midSigX) {
                        i++;
                        break;
                    }
                i++;
            }

            //nächsten Biarc berechnen
            if (approximation.isEmpty())
                biarc = BiarcUtils.getOptBiarc(currentBiarc,
                        getDirection(currentBiarc.get(0), firstExtremePoint, middlePoint),
                        peekDirectionAhead(input, i));
            else
                biarc = BiarcUtils.getOptBiarc(currentBiarc,
                        angleToDirection(approximation.get(approximation.size() - 1).getEndDirectionAngle()),
                        peekDirectionAhead(input, i));

            //optimize if activated
            if (withDistanceCriterion)
                while (BiarcUtils.getDistance(currentBiarc, biarc) > maxDistance && currentBiarc.size() > 2) {
                    i -= 1;
                    currentBiarc.remove(currentBiarc.size() - 1);
                    if (approximation.isEmpty())
                        biarc = BiarcUtils.getOptBiarc(currentBiarc,
                                peekDirectionAhead(input, (i - currentBiarc.size() + 2)),
                                peekDirectionAhead(input, i));
                    else
                        biarc = BiarcUtils.getOptBiarc(currentBiarc,
                                angleToDirection(approximation.get(approximation.size() - 1).getEndDirectionAngle()),
                                peekDirectionAhead(input, i));
                }

            assert biarc != null;

            approximation.add(biarc);

            i--;
            currentBiarc = new ArrayList<Point2D>();
            firstCurvature = 3;
            firstReady = false;
            secondReady = false;
            looping = false;
        }
    }

    /**
     * Die Methode approximiert den Steigungsvektor von Stelle i-1 in input, indem sie sich die nächsten Punkte ansieht
     */
    private Point2D.Double peekDirectionAhead(ArrayList<Point2D> input, int i) {
        int j = i, firstCurvature = 3, curCurvature;
        boolean finished = false;
        Point2D.Double curPoint, extremePoint = null;
        double startDirection, curDirection;
        while (j < input.size() && !finished) {
            curPoint = (Point2D.Double) input.get(j);
            //check curvature of graph
            curCurvature = getCurvature(input.get(j - 2),
                    input.get(j - 1), input.get(j));

            if (firstCurvature == 3)
                firstCurvature = curCurvature;
            else if (firstCurvature != curCurvature) {
                finished = true;
            }

            //save extreme point to compute start direction later
            if (firstCurvature == +1) {
                if (extremePoint == null)
                    extremePoint = curPoint;
                else if (
                        (-(curPoint.getY() - input.get(i - 1).getY())) / (curPoint.getX() - input.get(i - 1).getX()) >
                                (-(extremePoint.getY() - input.get(i - 1).getY())) / (extremePoint.getX() -
                                        input.get(i - 1).getX())
                        )
                    extremePoint = curPoint;
            } else if (firstCurvature == -1) {
                if (extremePoint == null)
                    extremePoint = curPoint;
                else if (
                        (-(curPoint.getY() - input.get(i - 1).getY())) / (curPoint.getX() - input.get(i - 1).getX()) <
                                (-(extremePoint.getY() - input.get(i - 1).getY())) / (extremePoint.getX() -
                                        input.get(i - 1).getX())
                        )
                    extremePoint = curPoint;
            }
            //loop detection
            if (extremePoint != null) {
                startDirection = (-(extremePoint.getY() - input.get(i - 1).getY())) / (extremePoint.getX() -
                        input.get(i - 1).getX());
                curDirection = (-(input.get(j).getY() -
                        input.get(j - 1).getY())) / (input.get(j).getX()
                        - input.get(j - 1).getX());
                if (startDirection < 0) {
                    if (curDirection > -startDirection || Math.signum(extremePoint.getX() -
                            input.get(i - 1).getX()) != Math.signum(input.get(j).getX()
                            - input.get(j - 1).getX())) {
                        break;
                    }
                } else {
                    if (curDirection < -startDirection || Math.signum(extremePoint.getX() -
                            input.get(i - 1).getX()) != Math.signum(input.get(j).getX()
                            - input.get(j - 1).getX())) {
                        break;
                    }
                }
            } else {
                break;
            }
            j++;
        }
        return getDirection(input.get(i - 2), extremePoint, input.get(i - 1));
    }

    private Point2D.Double angleToDirection(double endDirectionAngle) {
        return computeDirection(new Point2D.Double(0, 0),
                new Point2D.Double(Math.cos((endDirectionAngle * 2 * Math.PI) / 360), -Math.sin((endDirectionAngle * 2 * Math.PI) / 360)));
    }

    private Point2D.Double getDirection(Point2D p1, Point2D p2, Point2D pm) {
        if (p1 == null)
            return computeDirection(pm, p2);
        else if (p2 == null)
            return computeDirection(p1, pm);
        else
            return computeDirection(p1, p2);

    }

    private Point2D.Double computeDirection(Point2D p1, Point2D p2) {
        double dx = p2.getX() - p1.getX();
        double dy = -(p2.getY() - p1.getY());
        double length = Math.sqrt(dx * dx + dy * dy);

        return new Point2D.Double(dx / length, dy / length);
    }

    /**
     * Prüft, ob p3 links von, auf oder rechts von der Geraden durch p1 und p2 liegt.
     * Gibt entsprechend +1, 0 oder -1 zurück.
     *
     * @param p1 Point 1
     * @param p2 Point 2
     * @param p3 Point 3
     * @return Curvature
     */
    public int getCurvature(Point2D p1, Point2D p2, Point2D p3) {
        double x1, x2, x3, y1, y2, y3, curvature;
        x1 = p1.getX();
        y1 = p1.getY();
        x2 = p2.getX();
        y2 = p2.getY();
        x3 = p3.getX();
        y3 = p3.getY();

        curvature = (x2 - x1) * (y3 - y1) - (x3 - x1) * (y2 - y1);

        if (curvature < 0)
            return -1;
        else if (curvature == 0)
            return 0;
        else
            return 1;
    }
}
