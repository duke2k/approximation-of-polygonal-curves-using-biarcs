package core.state;

import com.google.inject.Inject;
import core.algorithm.BiarcApproximator;
import geom.Biarc;

import javax.annotation.Nonnull;
import java.awt.geom.Point2D;
import java.util.List;

/**
 * User: seickelberg
 * Date: 14.05.13
 */
public class BiarcListResultState extends PointListBasedState<List<Biarc>> {

    @Inject
    public BiarcListResultState(@Nonnull List<Point2D.Double> points, @Nonnull BiarcApproximator approximator) {
        super(points, approximator);
    }
}
