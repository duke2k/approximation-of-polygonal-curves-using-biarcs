package core.state;

import core.algorithm.Approximator;

import javax.annotation.Nonnull;

/**
 * User: seickelberg
 * Date: 03.04.13
 */
public interface State<I, O, A extends Approximator<I, O>> {

    @Nonnull
    A getApproximator();

    void setApproximator(@Nonnull A approximator);

    void approximate();

    void setStatusAttributeBy(@Nonnull String key, @Nonnull String value);

    void removeStatusAttributeBy(@Nonnull String key);

    @Nonnull
    String getCurrentStatusText();

    void clear();
}