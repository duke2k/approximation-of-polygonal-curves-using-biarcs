package core.state;

import core.algorithm.Approximator;
import ui.helper.StatusLine;

import javax.annotation.Nonnull;
import java.util.Date;
import java.util.Observable;

/**
 * User: Samuel
 * Date: 27.05.13
 */
public abstract class ObservableState<I, O, A extends Approximator<I, O>> extends Observable implements State<I, O, A> {

    protected StatusLine statusLine = StatusLine.getSingletonInstance();

    private Date lastModified;

    private A approximator;

    public ObservableState(@Nonnull A approximator) {
        this.approximator = approximator;
    }

    public void setChangedAndNotify() {
        setChanged();
        notifyObservers();
    }

    @Nonnull
    @Override
    public String getCurrentStatusText() {
        return statusLine.toString();
    }

    @Override
    public void setStatusAttributeBy(@Nonnull String key, @Nonnull String value) {
        statusLine.put(key, value);
    }

    @Override
    public void removeStatusAttributeBy(@Nonnull String key) {
        statusLine.remove(key);
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
        setStatusAttributeBy("last modified", lastModified.toString());
    }

    @Nonnull
    public A getApproximator() {
        return approximator;
    }

    public void setApproximator(@Nonnull A approximator) {
        this.approximator = approximator;
    }
}
