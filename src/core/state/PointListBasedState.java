package core.state;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import core.algorithm.Approximator;
import geom.Context2D;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.awt.geom.Point2D;
import java.util.Date;
import java.util.List;

import static com.google.common.collect.ImmutableList.copyOf;

/**
 * User: seickelberg
 * Date: 14.05.13
 */
public abstract class PointListBasedState<O> extends ObservableState<List<Point2D.Double>, O, Approximator<List<Point2D.Double>, O>> {

    private final List<Point2D.Double> points;

    private Context2D context;

    @Inject
    public PointListBasedState(@Nonnull List<Point2D.Double> points, @Nonnull Approximator<List<Point2D.Double>, O> delegate) {
        super(delegate);
        this.points = points;
    }

    @Override
    public void approximate() {
        getApproximator().executeWith(points);
        setStatusAttributeBy("points approximated", points.size() + "");
        setStatusAttributeBy("approximation", getApproximator().getStatusInfoText());
        setChangedAndNotify();
    }

    @Override
    public void clear() {
        removeAllPoints();
        getApproximator().clear();
        removeStatusAttributeBy("points approximated");
        removeStatusAttributeBy("approximation");
        removeStatusAttributeBy("last modified");
        setChangedAndNotify();
    }

    public void addPoint(@Nonnegative int x, @Nonnegative int y) {
        points.add(new Point2D.Double(x, y));
        createOrUpdateContext();
        setLastModified(new Date());
        removeStatusAttributeBy("points approximated");
        removeStatusAttributeBy("approximation");
        setChangedAndNotify();
    }

    public void addPoint(@Nonnegative int x, @Nonnegative int y, @Nullable Context2D context) {
        if (context == null) {
            addPoint(x, y);
        } else {
            addPoint(
                    (x - context.getxOffset()),
                    (y - context.getyOffset())
            );
        }
    }

    @Nonnull
    public List<Point2D.Double> getPoints() {
        return points;
    }

    @Nonnull
    public ImmutableList<Point2D.Double> getPointsAsImmutableList() {
        return copyOf(points);
    }

    public void removeAllPoints() {
        points.clear();
    }

    @Nullable
    public Context2D getContext() {
        return context;
    }

    private void createOrUpdateContext() {
        if (context == null) {
            context = Context2D.optimalContextFor(points);
        } else {
            context.update(points);
        }
    }
}
